CREATE TABLE if NOT EXISTS public.providers (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    user_id BIGINT NOT NULL,
    specialization VARCHAR(255) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT fk_user_provider FOREIGN KEY (user_id) REFERENCES users (id)
);

ALTER TABLE public.offerings DROP CONSTRAINT fk_user_offering;

ALTER TABLE public.offerings RENAME COLUMN user_id TO provider_id;

ALTER TABLE public.offerings ADD CONSTRAINT fk_provider_offering FOREIGN KEY (provider_id) REFERENCES public.providers (id);