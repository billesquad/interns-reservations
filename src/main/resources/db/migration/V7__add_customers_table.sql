CREATE TABLE if NOT EXISTS public.customers (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT fk_user_customer FOREIGN KEY (id) REFERENCES users (id)
);
