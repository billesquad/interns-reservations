CREATE TABLE if NOT EXISTS public.work_hours (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    provider_id BIGINT NOT NULL,
    day_of_week VARCHAR(9) NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE NULL,
    CONSTRAINT fk_provider_work_hours FOREIGN KEY (provider_id) REFERENCES public.providers (id)
);