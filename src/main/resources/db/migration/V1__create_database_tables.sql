CREATE SEQUENCE if NOT EXISTS public.id_seq AS bigint START WITH 1000;

CREATE TABLE if NOT EXISTS public.addresses (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    country VARCHAR(255),
    city VARCHAR(255),
    street VARCHAR(255),
    zip_code VARCHAR(10),
    building_number VARCHAR(10),
    apartment_number VARCHAR(10),
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE
    );

CREATE TABLE if NOT EXISTS public.users (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    address_id BIGINT NOT NULL,
    user_role VARCHAR(45) NOT NULL,
    status VARCHAR(45) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    birth_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    phone_number VARCHAR(15),
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT fk_user_addresses FOREIGN KEY (address_id) REFERENCES addresses (id)
);

CREATE TABLE if NOT EXISTS public.offerings (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    user_id BIGINT NOT NULL,
    offering_name VARCHAR(255) NOT NULL,
    is_active BOOLEAN NOT NULL,
    duration INT NOT NULL,
    price DECIMAL(9,2) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT fk_user_offering FOREIGN KEY (user_id) REFERENCES public.users (id)
);

CREATE TABLE if NOT EXISTS public.reservations (
    id BIGINT NOT NULL DEFAULT nextval('id_seq') PRIMARY KEY,
    user_id BIGINT NOT NULL,
    offering_id BIGINT NOT NULL,
    reservation_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    status VARCHAR(45) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT fk_user_reservation FOREIGN KEY (user_id) REFERENCES public.users (id),
    CONSTRAINT fk_offering_reservation FOREIGN KEY (offering_id) REFERENCES public.offerings (id)
);
