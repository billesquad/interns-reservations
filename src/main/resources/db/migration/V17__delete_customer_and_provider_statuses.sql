ALTER TABLE public.customers DROP status;
ALTER TABLE public.providers DROP status;

ALTER TABLE public.users ADD status VARCHAR(45) NOT NULL;
