ALTER TABLE public.users DROP COLUMN status;
ALTER TABLE public.customers ADD COLUMN status VARCHAR(45) NOT NULL;
ALTER TABLE public.providers ADD COLUMN status VARCHAR(45) NOT NULL;