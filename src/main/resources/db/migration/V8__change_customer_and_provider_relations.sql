ALTER TABLE public.providers ADD CONSTRAINT fk_user_provider FOREIGN KEY (id) REFERENCES users (id);

ALTER TABLE public.reservations DROP CONSTRAINT fk_user_reservation;
ALTER TABLE public.reservations RENAME COLUMN user_id TO customer_id;
ALTER TABLE public.reservations ADD CONSTRAINT fk_customer_reservation FOREIGN KEY (customer_id) REFERENCES public.customers (id);