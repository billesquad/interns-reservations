package com.billennium.internsreservations.exceptions;

public record ErrorMessage(String fieldName, String error) {

    @Override
    public String toString() {
        return error;
    }

}
