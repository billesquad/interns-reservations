package com.billennium.internsreservations.exceptions.reservation;

public class InvalidReservationStatusException extends IllegalArgumentException {

    private final String fieldName;

    public InvalidReservationStatusException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

}
