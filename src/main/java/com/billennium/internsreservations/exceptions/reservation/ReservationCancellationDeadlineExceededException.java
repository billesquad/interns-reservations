package com.billennium.internsreservations.exceptions.reservation;

public class ReservationCancellationDeadlineExceededException extends IllegalArgumentException {
    private final String fieldName;

    public ReservationCancellationDeadlineExceededException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

}
