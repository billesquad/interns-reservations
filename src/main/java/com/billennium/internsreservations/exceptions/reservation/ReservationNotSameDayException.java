package com.billennium.internsreservations.exceptions.reservation;

public class ReservationNotSameDayException extends IllegalArgumentException {

    private final String fieldName;

    public ReservationNotSameDayException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

}
