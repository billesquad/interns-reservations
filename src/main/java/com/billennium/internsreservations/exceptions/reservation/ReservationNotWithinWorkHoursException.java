package com.billennium.internsreservations.exceptions.reservation;

public class ReservationNotWithinWorkHoursException extends IllegalArgumentException {

    private final String fieldName;

    public ReservationNotWithinWorkHoursException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }

}
