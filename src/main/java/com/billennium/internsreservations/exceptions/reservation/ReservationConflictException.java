package com.billennium.internsreservations.exceptions.reservation;

public class ReservationConflictException extends IllegalArgumentException {

    private final String fieldName;

    public ReservationConflictException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }

}
