package com.billennium.internsreservations.exceptions.workhour;

public class InvalidWorkHourException extends IllegalArgumentException {

    private final String fieldName;

    public InvalidWorkHourException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }

}
