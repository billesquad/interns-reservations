package com.billennium.internsreservations.exceptions.workhour;

public class ProviderCalendarException extends RuntimeException {

    private final String fieldName;

    public ProviderCalendarException(String fieldName, String message) {
        super(message);
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }

}
