package com.billennium.internsreservations.exceptions;

import com.billennium.internsreservations.exceptions.offering.InvalidCurrencyException;
import com.billennium.internsreservations.exceptions.reservation.*;
import com.billennium.internsreservations.exceptions.security.GoogleTokenException;
import com.billennium.internsreservations.exceptions.security.UnauthorizedException;
import com.billennium.internsreservations.exceptions.user.UserAlreadyExistsException;
import com.billennium.internsreservations.exceptions.workhour.InvalidWorkHourException;
import com.billennium.internsreservations.exceptions.workhour.ProviderCalendarException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(InvalidCurrencyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage invalidCurrencyException(InvalidCurrencyException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage entityNotFoundException(EntityNotFoundException exception) {
        return new ErrorMessage(exception.getEntityName(), exception.getMessage());
    }

    @ExceptionHandler(value = EntityNotActiveException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage entityNotActiveException(EntityNotActiveException exception) {
        return new ErrorMessage(exception.getEntityName(), exception.getMessage());
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage userAlreadyExistsException(UserAlreadyExistsException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(value = InvalidWorkHourException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage invalidWorkHourException(InvalidWorkHourException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(ReservationNotSameDayException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage reservationNotSameDayException(ReservationNotSameDayException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(ReservationNotWithinWorkHoursException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage reservationNotWithinWorkHoursException(ReservationNotWithinWorkHoursException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }
    @ExceptionHandler(ReservationConflictException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage reservationConflictException(ReservationConflictException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(ProviderCalendarException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage providerCalendarException(ProviderCalendarException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException exception) {
        Map<String, String> errors = new HashMap<>();
        exception.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorMessage unauthorizedException(UnauthorizedException exception) {
        return new ErrorMessage(exception.getEntityName(), exception.getMessage());
    }

    @ExceptionHandler(GoogleTokenException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorMessage googleTokenVerificationException(GoogleTokenException exception) {
        return new ErrorMessage(exception.getEntityName(), exception.getMessage());
    }

    @ExceptionHandler(InvalidReservationStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage invalidReservationStatusException(InvalidReservationStatusException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }

    @ExceptionHandler(ReservationCancellationDeadlineExceededException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage reservationCancellationDeadlineExceededException(ReservationCancellationDeadlineExceededException exception) {
        return new ErrorMessage(exception.getFieldName(), exception.getMessage());
    }
}
