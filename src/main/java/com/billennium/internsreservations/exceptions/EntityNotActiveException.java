package com.billennium.internsreservations.exceptions;

public class EntityNotActiveException extends RuntimeException {
    
    private final String entityName;
    
    public EntityNotActiveException(String entityName, String message) {
        super(message);
        this.entityName = entityName;
    }
    public String getEntityName() {
        return entityName;
    }

}
