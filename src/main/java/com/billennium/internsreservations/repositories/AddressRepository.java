package com.billennium.internsreservations.repositories;

import com.billennium.internsreservations.models.address.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
