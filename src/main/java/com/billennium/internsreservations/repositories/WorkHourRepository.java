package com.billennium.internsreservations.repositories;

import com.billennium.internsreservations.models.workhour.WorkHour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface WorkHourRepository extends JpaRepository<WorkHour, Long> {

    @Query("""
            SELECT w
            FROM WorkHour w
            WHERE w.provider.id = :providerId
            AND w.startTime IS NOT NULL
            AND w.endTime IS NOT NULL
            """)
    List<WorkHour> getProviderFilledCalendar(@Param("providerId") Long providerId);

    @Query("""
            SELECT w
            FROM WorkHour w
            WHERE w.provider.id = :providerId
            """)
    List<WorkHour> getProviderCalendar(@Param("providerId") Long providerId);

    @Query("""
            SELECT w.provider.email
            FROM WorkHour w
            WHERE w.id = :workHourId
            """)
    Optional<String> getProviderEmailByWorkHourId(@Param("workHourId") Long workHourId);

}
