package com.billennium.internsreservations.repositories;

import com.billennium.internsreservations.models.offering.Offering;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface OfferingRepository extends JpaRepository<Offering, Long>, JpaSpecificationExecutor<Offering> {

    @Query("""
            SELECT o.provider.email
            FROM Offering o
            WHERE o.id = :offeringId
            """)
    Optional<String> getProviderEmailByOfferingId(@Param("offeringId") Long offeringId);

    Page<Offering> findAllByProviderIdAndIsActiveTrue(Long providerId, PageRequest pageRequest);
}
