package com.billennium.internsreservations.repositories;

import com.billennium.internsreservations.models.reservation.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Long>, JpaSpecificationExecutor<Reservation> {

    @Query("""
            SELECT r
            FROM Reservation r
            WHERE r.offering.provider.id = :providerId
            AND FUNCTION('DATE', r.reservationDate) = :date
            """)
    List<Reservation> findReservationsByProviderAndDay(@Param("providerId") Long providerId,
                                                       @Param("date") LocalDate date);

    @Query("""
            SELECT r.customer.email
            FROM Reservation r
            WHERE r.id = :reservationId
            """)
    Optional<String> getCustomerEmailByReservationId(@Param("reservationId") Long reservationId);

    @Query("""
            SELECT r.offering.provider.email
            FROM Reservation r
            WHERE r.id = :reservationId
            """)
    Optional<String> getProviderEmailByReservationId(@Param("reservationId") Long reservationId);

}
