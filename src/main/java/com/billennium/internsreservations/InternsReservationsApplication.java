package com.billennium.internsreservations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication
@EnableJpaAuditing
public class InternsReservationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(InternsReservationsApplication.class, args);
    }

}
