package com.billennium.internsreservations.services.workhour;

import com.billennium.internsreservations.models.workhour.dtos.WorkHourMapper;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourResponseDTO;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourUpdateDTO;
import com.billennium.internsreservations.services.offering.OfferingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

import static com.billennium.internsreservations.models.workhour.dtos.WorkHourMapper.mapToWorkHourResponseDTO;

@Component
@RequiredArgsConstructor
public class WorkHourFacade {

    private final WorkHourService workHourService;
    private final OfferingService offeringService;

    public WorkHourResponseDTO getWorkHourById(Long workHourId) {
        return mapToWorkHourResponseDTO(workHourService.getWorkHourById(workHourId));
    }

    public List<WorkHourResponseDTO> getProviderCalendar(Long providerId) {
        return workHourService.getProviderCalendar(providerId)
                .stream()
                .sorted(Comparator.comparing(workHour -> workHour.getDayOfWeek().ordinal()))
                .map(WorkHourMapper::mapToWorkHourResponseDTO)
                .toList();
    }
    public List<WorkHourResponseDTO> getProviderCalendarByOffering(Long offeringId) {
        Long providerId = offeringService.getOfferingById(offeringId).getProvider().getId();
        return workHourService.getProviderCalendar(providerId)
                .stream()
                .sorted(Comparator.comparing(workHour -> workHour.getDayOfWeek().ordinal()))
                .map(WorkHourMapper::mapToWorkHourResponseDTO)
                .toList();
    }

    public WorkHourResponseDTO updateWorkHour(Long workHourId, WorkHourUpdateDTO workHourUpdateDTO) {
        return mapToWorkHourResponseDTO(workHourService.updateWorkHour(workHourId, workHourUpdateDTO));
    }

    public void deleteWorkHour(Long workHourId) {
        workHourService.deleteWorkHourById(workHourId);
    }

}
