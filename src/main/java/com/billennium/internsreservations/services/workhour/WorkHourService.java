package com.billennium.internsreservations.services.workhour;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.exceptions.workhour.InvalidWorkHourException;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.workhour.WorkHour;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourUpdateDTO;
import com.billennium.internsreservations.repositories.WorkHourRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.billennium.internsreservations.models.workhour.WorkHour.MINIMAL_DURATION;

@Service
@RequiredArgsConstructor
public class WorkHourService {

    private final WorkHourRepository workHourRepository;

    public WorkHour getWorkHourById(Long workHourId) {
        return workHourRepository.findById(workHourId)
                .orElseThrow(() -> new EntityNotFoundException("Work hour", "No work hour found with id: " + workHourId));
    }

    public List<WorkHour> getProviderCalendar(Long providerId) {
        return workHourRepository.getProviderCalendar(providerId);
    }
    public List<WorkHour> getProviderFilledCalendar(Long providerId) {
        return workHourRepository.getProviderFilledCalendar(providerId);
    }

    public WorkHour updateWorkHour(Long workHourId, WorkHourUpdateDTO workHourUpdateDTO) {
        LocalTime startTime = workHourUpdateDTO.startTime();
        LocalTime endTime = workHourUpdateDTO.endTime();

        if (!isWorkHourValid(startTime, endTime)) {
            throw new InvalidWorkHourException("Work hour", "Start time must be earlier than the end time.");
        }
        if (!isMinimumDuration(startTime, endTime, MINIMAL_DURATION)) {
            throw new InvalidWorkHourException("Work hour", "There must be " + MINIMAL_DURATION + " hour(s) between the start time and the end time.");
        }

        WorkHour workHour = getWorkHourById(workHourId);
        workHour.setStartTime(startTime);
        workHour.setEndTime(endTime);
        return workHourRepository.saveAndFlush(workHour);
    }

    private boolean isMinimumDuration(LocalTime startTime, LocalTime endTime, Integer minimalDuration) {
        return startTime.isBefore(endTime.minusHours(minimalDuration));
    }

    private boolean isWorkHourValid(LocalTime startTime, LocalTime endTime) {
        return startTime.isBefore(endTime);
    }

    public List<WorkHour> createEmptyCalendar(Provider provider) {
        Set<WorkHour> calendar = new HashSet<>();

        for (DayOfWeek day : DayOfWeek.values()) {
            WorkHour workHour = new WorkHour();
            workHour.setDayOfWeek(day);
            workHour.setProvider(provider);
            calendar.add(workHour);
        }
        return workHourRepository.saveAllAndFlush(calendar);
    }

    public void deleteWorkHourById(Long workHourId) {
        WorkHour workHour = getWorkHourById(workHourId);
        workHour.setStartTime(null);
        workHour.setEndTime(null);
        workHourRepository.saveAndFlush(workHour);
    }

    public String getProviderEmailByWorkHourId(Long workHourId) {
        return workHourRepository.getProviderEmailByWorkHourId(workHourId)
                .orElseThrow(() -> new EntityNotFoundException("Work hour", "No work hour found with id: " + workHourId));
    }

}
