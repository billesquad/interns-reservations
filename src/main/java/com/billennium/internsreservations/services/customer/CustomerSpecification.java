package com.billennium.internsreservations.services.customer;

import com.billennium.internsreservations.models.user.enums.Status;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.Reservation;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CustomerSpecification {

    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static Specification<Customer> firstNameContains(String firstName) {
        return (root, query, cb) -> firstName == null ? null :
                cb.like(cb.lower(root.get("firstName")), "%" + firstName.toLowerCase() + "%");
    }

    public static Specification<Customer> lastNameContains(String lastName) {
        return (root, query, cb) -> lastName == null ? null :
                cb.like(cb.lower(root.get("lastName")), "%" + lastName.toLowerCase() + "%");
    }

    public static Specification<Customer> emailContains(String email) {
        return (root, query, cb) -> email == null ? null :
                cb.like(cb.lower(root.get("email")), "%" + email.toLowerCase() + "%");
    }

    public static Specification<Customer> phoneNumberContains(String phoneNumber) {
        return (root, query, cb) -> phoneNumber == null ? null :
                cb.like(cb.lower(root.get("phoneNumber")), "%" + phoneNumber.toLowerCase() + "%");
    }

    public static Specification<Customer> birthDateEquals(String birthDate) {
        return (root, query, cb) -> cb.equal(root.get("birthDate"), LocalDate.parse(birthDate, dateTimeFormatter));
    }

    public static Specification<Customer> statusEquals(String status) {
        return (root, query, cb) -> {
            Status enumStatus = Status.valueOf(status);
            return cb.equal(root.get("status"), enumStatus);
        };
    }

    public static Specification<Customer> customersByProviderId(Long providerId) {
        return (root, query, cb) -> {
            Subquery<Long> subquery = query.subquery(Long.class);
            Root<Customer> subRoot = subquery.from(Customer.class);
            Join<Customer, Reservation> reservationJoin = subRoot.join("reservations", JoinType.INNER);
            Join<Reservation, Offering> offeringJoin = reservationJoin.join("offering", JoinType.INNER);
            subquery.select(subRoot.get("id"))
                    .where(cb.equal(offeringJoin.get("provider").get("id"), providerId));

            Predicate customerInSubquery = root.get("id").in(subquery);

            return cb.and(customerInSubquery);
        };
    }
}


