package com.billennium.internsreservations.services.customer;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.customer.dtos.CustomerInputDTO;
import com.billennium.internsreservations.models.customer.dtos.CustomerRequestDTO;
import com.billennium.internsreservations.models.user.User;
import com.billennium.internsreservations.models.user.enums.Status;
import com.billennium.internsreservations.repositories.CustomerRepository;
import com.billennium.internsreservations.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final UserRepository userRepository;

    public Customer getCustomerById(Long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(() -> new EntityNotFoundException("Customer", "No customer found with id: " + customerId));
    }

    public Customer createCustomer(Address address, CustomerInputDTO customerInputDTO, User user) {
        Customer customer = new Customer(
                address,
                customerInputDTO.getFirstName(),
                customerInputDTO.getLastName(),
                customerInputDTO.getBirthDate(),
                user.getEmail(),
                customerInputDTO.getPhoneNumber(),
                user.getAccessToken(),
                user.getRefreshToken(),
                user.getIdToken());
        userRepository.delete(user);
        return customerRepository.saveAndFlush(customer);
    }

    public void deleteCustomerById(Long customerId) {
        Customer customer = getCustomerById(customerId);
        customer.setStatus(Status.INACTIVE);
        customerRepository.saveAndFlush(customer);
    }

    public Customer updateCustomerById(Long customerId, CustomerInputDTO customerInputDTO) {
        Customer customer = getCustomerById(customerId);
        customer.setFirstName(customerInputDTO.getFirstName());
        customer.setLastName(customerInputDTO.getLastName());
        customer.setBirthDate(customerInputDTO.getBirthDate());
        customer.setPhoneNumber(customerInputDTO.getPhoneNumber());

        return customerRepository.saveAndFlush(customer);
    }

    public Page<Customer> getAllCustomersIdByProvider(Long providerId, CustomerRequestDTO customerRequestDTO) {
        PageRequest pageRequest = PageRequest.of(
                Integer.parseInt(customerRequestDTO.getPage()),
                Integer.parseInt(customerRequestDTO.getSize()),
                Sort.Direction.valueOf(customerRequestDTO.getDirection()),
                customerRequestDTO.getSortParam());

        Specification<Customer> spec = Specification.where(null);

        if (customerRequestDTO.getFirstName() != null) {
            spec = spec.and(CustomerSpecification.firstNameContains(customerRequestDTO.getFirstName()));
        }
        if (customerRequestDTO.getLastName() != null) {
            spec = spec.and(CustomerSpecification.lastNameContains(customerRequestDTO.getLastName()));
        }
        if (customerRequestDTO.getEmail() != null) {
            spec = spec.and(CustomerSpecification.emailContains(customerRequestDTO.getEmail()));
        }
        if (customerRequestDTO.getPhoneNumber() != null) {
            spec = spec.and(CustomerSpecification.phoneNumberContains(customerRequestDTO.getPhoneNumber()));
        }
        if (customerRequestDTO.getBirthDate() != null) {
            spec = spec.and(CustomerSpecification.birthDateEquals(customerRequestDTO.getBirthDate()));
        }
        if (customerRequestDTO.getStatus() != null) {
            spec = spec.and(CustomerSpecification.statusEquals(customerRequestDTO.getStatus()));
        }
        if (providerId != null) {
            spec = spec.and(CustomerSpecification.customersByProviderId(providerId));
        }

        return customerRepository.findAll(spec, pageRequest);
    }

}
