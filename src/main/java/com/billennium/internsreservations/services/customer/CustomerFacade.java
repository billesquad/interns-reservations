package com.billennium.internsreservations.services.customer;

import com.billennium.internsreservations.exceptions.user.UserAlreadyExistsException;
import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.customer.dtos.CustomerInputDTO;
import com.billennium.internsreservations.models.customer.dtos.CustomerResponseDTO;
import com.billennium.internsreservations.models.user.User;
import com.billennium.internsreservations.models.user.enums.Status;
import com.billennium.internsreservations.services.address.AddressService;
import com.billennium.internsreservations.services.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.billennium.internsreservations.models.customer.dtos.CustomerMapper.mapToCustomerResponseDTO;

@Component
@RequiredArgsConstructor
public class CustomerFacade {

    private final CustomerService customerService;
    private final UserService userService;
    private final AddressService addressService;

    public CustomerResponseDTO createCustomer(CustomerInputDTO customerInputDTO, String email) {
        User user = userService.getUserByEmail(email);
        if (user.getStatus() == Status.REGISTRATION_INCOMPLETE) {
            Address emptyAddress = addressService.createEmptyAddress();
            return mapToCustomerResponseDTO(customerService.createCustomer(emptyAddress, customerInputDTO, user));
        }
        throw new UserAlreadyExistsException("User", "User already registered.");
    }

    public void deleteCustomerById(Long customerId) {
        customerService.deleteCustomerById(customerId);
    }

    public CustomerResponseDTO updateCustomerById(Long id, CustomerInputDTO customerInputDTO) {
        Customer customer = customerService.updateCustomerById(id, customerInputDTO);
        return mapToCustomerResponseDTO(customer);
    }

    public CustomerResponseDTO getCustomerById(Long customerId) {
        return mapToCustomerResponseDTO(customerService.getCustomerById(customerId));
    }

}
