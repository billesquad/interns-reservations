package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.reservation.dtos.*;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import com.billennium.internsreservations.models.workhour.WorkHour;
import com.billennium.internsreservations.services.customer.CustomerService;
import com.billennium.internsreservations.services.offering.OfferingService;
import com.billennium.internsreservations.services.workhour.WorkHourService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static com.billennium.internsreservations.models.reservation.dtos.ReservationMapper.toReservationDTO;

@Component
@RequiredArgsConstructor
public class ReservationFacade {

    private final ReservationService reservationService;
    private final CustomerService customerService;
    private final OfferingService offeringService;
    private final WorkHourService workHourService;

    public Page<ReservationResponseDTO> getProviderReservations(Long providerId, ReservationRequestDTO reservationDTO) {
        return reservationService.getAllReservationsByProvider(providerId, reservationDTO).map(ReservationMapper::toReservationDTO);
    }

    public ReservationResponseDTO createReservation(ReservationCreateDTO reservationCreateDTO) {
        Customer customer = customerService.getCustomerById(reservationCreateDTO.customerId());
        Offering offering = offeringService.getOfferingById(reservationCreateDTO.offeringId());
        LocalDateTime reservationStartDate = reservationCreateDTO.reservationStartDate();
        List<WorkHour> providerCalendar = workHourService.getProviderFilledCalendar(offering.getProvider().getId());

        Reservation reservation = reservationService.createReservation(customer, offering, reservationStartDate, providerCalendar);
        return toReservationDTO(reservation);
    }

    public ReservationResponseDTO changeReservationStatus(Long reservationId, String status) {
        return toReservationDTO(reservationService.changeReservationStatus(reservationId, status));
    }

    public ReservationResponseDTO cancelReservationById(Long reservationId) {
        return toReservationDTO(reservationService.cancelReservationById(reservationId));
    }

    public Page<ReservationResponseDTO> getCustomerReservations(Long customerId, ReservationCustomerRequestDTO reservationCustomerRequestDto) {
        //This is only here to check if consumer exists
        customerService.getCustomerById(customerId);
        return reservationService.getCustomerReservations(customerId, reservationCustomerRequestDto).map(ReservationMapper::toReservationDTO);
    }

    public List<ReservationTimeDTO> getReservationsByProviderAndDay(Long providerId, LocalDate date) {
        return reservationService.getReservationsByProviderAndDay(providerId, date)
                .stream()
                .filter(
                        reservation ->
                                !(reservation.getStatus().equals(ReservationStatus.CANCELLED)
                                        || reservation.getStatus().equals(ReservationStatus.DECLINED)))
                .map(ReservationMapper::toReservationTimeDTO)
                .toList();
    }

    public ReservationResponseDTO getReservationById(Long reservationId) {
        return toReservationDTO(reservationService.getReservationById(reservationId));
    }

}
