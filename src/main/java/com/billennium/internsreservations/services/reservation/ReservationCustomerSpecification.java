package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class ReservationCustomerSpecification {
    public static Specification<Reservation> hasStatus(List<String> statuses) {
        return (root, query, criteriaBuilder) -> {
            if (statuses != null && !statuses.isEmpty()) {
                CriteriaBuilder.In<ReservationStatus> inClause = criteriaBuilder.in(root.get("status"));
                for (String status : statuses) {
                    inClause.value(ReservationStatus.valueOf(status));
                }
                return inClause;
            }
            return null;
        };
    }

    public static Specification<Reservation> hasCustomerId(Long customerId) {
        return (root, query, criteriaBuilder) -> {
            if (customerId != null) {
                return criteriaBuilder.equal(root.get("customer").get("id"), customerId);
            }
            return null;
        };
    }
}
