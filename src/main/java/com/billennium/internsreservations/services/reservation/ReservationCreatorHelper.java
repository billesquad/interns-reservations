package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.exceptions.reservation.ReservationConflictException;
import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.workhour.WorkHour;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public class ReservationCreatorHelper {

    public static boolean isOnTheSameDay(LocalDateTime reservationStartDate, LocalDateTime reservationEndDate) {
        return reservationStartDate.toLocalDate().equals(reservationEndDate.toLocalDate());
    }

    public static boolean isReservationTimeInWorkHourRange(LocalDateTime startDate, LocalDateTime endDate, List<WorkHour> providerCalendar) {
        DayOfWeek startDateDayOfWeek = startDate.getDayOfWeek();
        LocalTime startTime = startDate.toLocalTime();
        LocalTime endTime = endDate.toLocalTime();

        return providerCalendar
                .stream()
                .filter(workHour -> workHour.getDayOfWeek().equals(startDateDayOfWeek))
                .anyMatch(workHour -> isTimeInWorkHourRange(workHour, startTime, endTime));
    }

    public static void validateReservationOverlaps(LocalDateTime newReservationStart, LocalDateTime newReservationEnd, List<Reservation> reservations) {
        for (Reservation existingReservation : reservations) {
            LocalDateTime existingReservationStart = existingReservation.getReservationDate();
            LocalDateTime existingReservationEnd = existingReservation.getReservationDateEnd();

            if (newReservationStart.isBefore(existingReservationEnd) && newReservationEnd.isAfter(existingReservationStart)){
                throw new ReservationConflictException("Reservation", "This reservation overlaps with an existing reservation");
            }
        }
    }

    private static boolean isTimeInWorkHourRange(WorkHour workHour, LocalTime startTime, LocalTime endTime) {
        return (workHour.getStartTime().isBefore(startTime) || workHour.getStartTime().equals(startTime))
                && (workHour.getEndTime().isAfter(endTime) || workHour.getEndTime().equals(endTime));
    }
}
