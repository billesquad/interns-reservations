package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.exceptions.EntityNotActiveException;
import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.exceptions.reservation.ReservationCancellationDeadlineExceededException;
import com.billennium.internsreservations.exceptions.reservation.InvalidReservationStatusException;
import com.billennium.internsreservations.exceptions.reservation.ReservationNotSameDayException;
import com.billennium.internsreservations.exceptions.reservation.ReservationNotWithinWorkHoursException;
import com.billennium.internsreservations.exceptions.workhour.ProviderCalendarException;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.reservation.dtos.ReservationCustomerRequestDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationRequestDTO;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import com.billennium.internsreservations.models.workhour.WorkHour;
import com.billennium.internsreservations.repositories.ReservationRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static com.billennium.internsreservations.services.reservation.ReservationCreatorHelper.*;

@Component
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepository reservationRepository;

    public Reservation getReservationById(Long reservationId) {
        return reservationRepository.findById(reservationId)
                .orElseThrow(() -> new EntityNotFoundException("Reservation", "No reservation found with id: " + reservationId));
    }

    public Page<Reservation> getAllReservationsByProvider(Long providerId, ReservationRequestDTO reservationRequestDTO) {
        PageRequest pageRequest = PageRequest.of(
                Integer.parseInt(reservationRequestDTO.getPage()),
                Integer.parseInt(reservationRequestDTO.getSize()),
                Sort.Direction.valueOf(reservationRequestDTO.getDirection()),
                reservationRequestDTO.getSortParam());

        Specification<Reservation> spec = Specification.where(null);

        if (reservationRequestDTO.getCustomer() != null) {
            spec = spec.and(ReservationSpecification.customerLastNameContains(reservationRequestDTO.getCustomer()));
        }
        if (reservationRequestDTO.getOffering() != null) {
            spec = spec.and(ReservationSpecification.offeringNameContains(reservationRequestDTO.getOffering()));
        }
        if (reservationRequestDTO.getReservationDate() != null && reservationRequestDTO.getReservationDateEnd() != null) {
            spec = spec.and(ReservationSpecification.datesBetween(reservationRequestDTO.getReservationDate(), reservationRequestDTO.getReservationDateEnd()));
        }
        if (reservationRequestDTO.getStatuses() != null) {
            spec = spec.and(ReservationCustomerSpecification.hasStatus(reservationRequestDTO.getStatuses()));
        }
        if (providerId != null) {
            spec = spec.and(ReservationSpecification.getQueryForSpecificProvider(providerId));
        }

        return reservationRepository.findAll(spec, pageRequest);
    }

    public List<Reservation> getReservationsByProviderAndDay(Long providerId, LocalDate date) {
        return reservationRepository.findReservationsByProviderAndDay(providerId, date);
    }

    @Transactional
    public Reservation createReservation(Customer customer, Offering offering, LocalDateTime reservationStartDate, List<WorkHour> providerCalendar) {
        if (!offering.getIsActive()) {
            throw new EntityNotActiveException("Offering", "This offering is not active.");
        }
        if (providerCalendar.isEmpty()) {
            throw new ProviderCalendarException("Work Hour", "Provider has no filled work hours.");
        }

        LocalDateTime reservationEndDate = reservationStartDate.plusMinutes(offering.getDuration());

        if (!isOnTheSameDay(reservationStartDate, reservationEndDate)) {
            throw new ReservationNotSameDayException("start time", "The end time of the reservation is not the same day as the start time.");
        }
        if (!isReservationTimeInWorkHourRange(reservationStartDate, reservationEndDate, providerCalendar)) {
            throw new ReservationNotWithinWorkHoursException("start time", "Reservation time is not within working hours or the reservation day is not a working day");
        }

        List<Reservation> reservations = getReservationsByProviderAndDay(offering.getProvider().getId(), reservationStartDate.toLocalDate());
        validateReservationOverlaps(reservationStartDate, reservationEndDate, reservations);

        Reservation reservation = new Reservation(offering, customer, reservationStartDate, reservationEndDate);
        return reservationRepository.saveAndFlush(reservation);
    }

    public Reservation changeReservationStatus(Long reservationId, String status) {
        Reservation reservation = getReservationById(reservationId);

        if (!ReservationStatus.isValid(status.toUpperCase())){
            throw new InvalidReservationStatusException("Status", "Provided reservation status is invalid.");
        }

        reservation.setStatus(ReservationStatus.valueOf(status));
        return reservationRepository.saveAndFlush(reservation);
    }

    public Reservation cancelReservationById(Long reservationId) {
        Reservation reservationToCancel = getReservationById(reservationId);
        if (!(LocalDate.now()).isBefore(reservationToCancel.getReservationDate().toLocalDate())){
            throw new ReservationCancellationDeadlineExceededException("Reservation", "A reservation must be cancelled at least one day before the scheduled date for reservation id : " + reservationId);
        }
        reservationToCancel.setStatus(ReservationStatus.CANCELLED);
        return reservationRepository.saveAndFlush(reservationToCancel);
    }

    public Page<Reservation> getCustomerReservations(Long customerId, ReservationCustomerRequestDTO reservationCustomerRequestDto) {
        PageRequest pageRequest = PageRequest.of(
                Integer.parseInt(reservationCustomerRequestDto.getPage()),
                Integer.parseInt(reservationCustomerRequestDto.getSize()),
                Sort.Direction.valueOf(reservationCustomerRequestDto.getDirection()),
                reservationCustomerRequestDto.getSortParam());

        Specification<Reservation> statusSpec = ReservationCustomerSpecification.hasStatus(reservationCustomerRequestDto.getStatuses());
        Specification<Reservation> customerSpec = ReservationCustomerSpecification.hasCustomerId(customerId);
        Specification<Reservation> combinedSpec = Specification.where(statusSpec).and(customerSpec);

        return reservationRepository.findAll(combinedSpec, pageRequest);
    }

    public String getCustomerEmailByReservationId(Long reservationId) {
        return reservationRepository.getCustomerEmailByReservationId(reservationId)
                .orElseThrow(() -> new EntityNotFoundException("Customer", "No reservation found with id: " + reservationId));
    }

    public String getProviderEmailByReservationId(Long reservationId) {
        return reservationRepository.getProviderEmailByReservationId(reservationId)
                .orElseThrow(() -> new EntityNotFoundException("Provider", "No reservation found with id: " + reservationId));
    }

}
