package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.Reservation;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;

public class ReservationSpecification {

    public static Specification<Reservation> datesBetween(String date, String dateEnd) {
        return (root, query, cb) -> date == null ? null : cb.between(root.get("reservationDate"),
                LocalDateTime.parse(date), LocalDateTime.parse(dateEnd));
    }

    public static Specification<Reservation> customerLastNameContains(String customerLastName) {
        return (root, query, cb) -> {
            Join<Reservation, Offering> customerJoin = root.join("customer");
            return cb.like(cb.lower(customerJoin.get("lastName")), "%" + customerLastName.toLowerCase() + "%");
        };
    }

    public static Specification<Reservation> offeringNameContains(String offeringName) {
        return (root, query, cb) -> {
            Join<Reservation, Offering> offeringJoin = root.join("offering");
            return cb.like(cb.lower(offeringJoin.get("offeringName")), "%" + offeringName.toLowerCase() + "%");
        };
    }

    public static Specification<Reservation> getQueryForSpecificProvider(Long providerId) {
        return (root, query, criteriaBuilder) -> {
            Join<Reservation, Offering> offeringJoin = root.join("offering");
            return criteriaBuilder.equal(offeringJoin.get("provider").get("id"), providerId);
        };
    }
}
