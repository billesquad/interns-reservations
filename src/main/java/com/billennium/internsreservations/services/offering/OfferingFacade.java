package com.billennium.internsreservations.services.offering;

import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.offering.dtos.*;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.services.provider.ProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.billennium.internsreservations.models.offering.dtos.OfferingMapper.mapToOfferingResponseDTO;

@Component
@RequiredArgsConstructor
public class OfferingFacade {

    private final OfferingService offeringService;
    private final ProviderService providerService;

    public OfferingResponseDTO createOffering(OfferingInputDTO offeringInputDTO) {
        Provider provider = providerService.getProviderById(offeringInputDTO.providerId());
        return mapToOfferingResponseDTO(offeringService.createOffering(offeringInputDTO, provider));
    }

    public OfferingResponseDTO getOfferingById(Long offeringId) {
        Offering offering = offeringService.getOfferingById(offeringId);
        return mapToOfferingResponseDTO(offering);
    }

    public void deleteOfferingById(Long offeringId) {
        offeringService.deleteOfferingById(offeringId);
    }

    public Page<OfferingResponseDTO> getOfferings(OfferingRequestDTO offeringRequestDTO) {
        return offeringService.getOfferings(offeringRequestDTO).map(OfferingMapper::mapToOfferingResponseDTO);
    }
    public Page<OfferingResponseDTO> getOfferingsByProviderId(Long providerId, OfferingProviderRequestDTO offeringProviderRequestDTO) {
        return offeringService.getOfferingsByProviderId(providerId, offeringProviderRequestDTO).map(OfferingMapper::mapToOfferingResponseDTO);
    }

    public OfferingResponseDTO updateOfferingById(Long offeringId, OfferingInputDTO offeringInputDTO) {
        Offering updatedOffering = offeringService.updateOfferingById(offeringId, offeringInputDTO);
        return mapToOfferingResponseDTO(updatedOffering);
    }

}
