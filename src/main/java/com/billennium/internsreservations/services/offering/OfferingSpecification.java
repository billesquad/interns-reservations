package com.billennium.internsreservations.services.offering;

import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.provider.Provider;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;

public class OfferingSpecification {

    public static Specification<Offering> nameContains(String name) {
        return (root, query, cb) -> name == null ? null :
                cb.like(cb.lower(root.get("offeringName")), "%" + name.toLowerCase() + "%");
    }

    public static Specification<Offering> priceGreaterThanOrEqual(String price) {
        return (root, query, cb) -> price == null ? null : cb.greaterThanOrEqualTo(root.get("price"),
                new BigDecimal(price));
    }

    public static Specification<Offering> priceLessThanOrEqual(String price) {
        return (root, query, cb) -> price == null ? null : cb.lessThanOrEqualTo(root.get("price"),
                new BigDecimal(price));
    }

    public static Specification<Offering> providerNameEquals(String specialization) {
        return (root, query, cb) -> {
            Join<Offering, Provider> providerJoin = root.join("provider");
            return cb.like(cb.lower(providerJoin.get("specialization")), "%" + specialization.toLowerCase() + "%");
        };
    }
}
