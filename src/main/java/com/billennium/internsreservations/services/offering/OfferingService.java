package com.billennium.internsreservations.services.offering;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.exceptions.offering.InvalidCurrencyException;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.offering.dtos.OfferingInputDTO;
import com.billennium.internsreservations.models.offering.dtos.OfferingProviderRequestDTO;
import com.billennium.internsreservations.models.offering.dtos.OfferingRequestDTO;
import com.billennium.internsreservations.models.offering.enums.Currency;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.repositories.OfferingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OfferingService {

    private final OfferingRepository offeringRepository;

    public Offering getOfferingById(Long offeringId) {
        return offeringRepository.findById(offeringId)
                .orElseThrow(() -> new EntityNotFoundException("Offering", "No offering found with id: " + offeringId));
    }

    public void deleteOfferingById(Long offeringId) {
        Offering offering = getOfferingById(offeringId);
        offering.setIsActive(false);
        offeringRepository.saveAndFlush(offering);
    }

    public Offering createOffering(OfferingInputDTO offeringInputDTO, Provider provider) {
        if (!Currency.isValid(offeringInputDTO.currency())) {
            throw new InvalidCurrencyException("currency", "Invalid currency: " + offeringInputDTO.currency());
        }
        Currency currency = Currency.valueOf(offeringInputDTO.currency());
        Offering offering = new Offering(provider,
                offeringInputDTO.offeringName(),
                offeringInputDTO.description(),
                offeringInputDTO.duration(),
                offeringInputDTO.price(),
                currency);
        return offeringRepository.saveAndFlush(offering);
    }

    public Page<Offering> getOfferings(OfferingRequestDTO offeringRequestDTO) {
        PageRequest pageRequest = PageRequest.of(
                Integer.parseInt(offeringRequestDTO.getPage()),
                Integer.parseInt(offeringRequestDTO.getSize()),
                Sort.Direction.valueOf(offeringRequestDTO.getDirection()),
                offeringRequestDTO.getSortParam());

        Specification<Offering> spec = Specification.where(null);
        spec = spec.and((root, query, criteriaBuilder) ->
                criteriaBuilder.isTrue(root.get("isActive")));

        if (offeringRequestDTO.getNameSearch() != null) {
            spec = spec.and(OfferingSpecification.nameContains(offeringRequestDTO.getNameSearch()));
        }
        if (offeringRequestDTO.getPriceFrom() != null) {
            spec = spec.and(OfferingSpecification.priceGreaterThanOrEqual(offeringRequestDTO.getPriceFrom()));
        }
        if (offeringRequestDTO.getPriceTo() != null) {
            spec = spec.and(OfferingSpecification.priceLessThanOrEqual(offeringRequestDTO.getPriceTo()));
        }
        if (offeringRequestDTO.getProvider() != null) {
            spec = spec.and(OfferingSpecification.providerNameEquals(offeringRequestDTO.getProvider()));
        }
        return offeringRepository.findAll(spec, pageRequest);
    }

    public Page<Offering> getOfferingsByProviderId(Long providerId, OfferingProviderRequestDTO offeringProviderRequestDTO) {
        PageRequest pageRequest = PageRequest.of(
                Integer.parseInt(offeringProviderRequestDTO.getPage()),
                Integer.parseInt(offeringProviderRequestDTO.getSize()),
                Sort.Direction.valueOf(offeringProviderRequestDTO.getDirection()),
                offeringProviderRequestDTO.getSortParam());
        return offeringRepository.findAllByProviderIdAndIsActiveTrue(providerId, pageRequest);
    }
    public Offering updateOfferingById(Long offeringId, OfferingInputDTO offeringInputDTO) {
        if (!Currency.isValid(offeringInputDTO.currency())) {
            throw new InvalidCurrencyException("currency", "Invalid currency: " + offeringInputDTO.currency());
        }
        Offering offering = getOfferingById(offeringId);
        offering.setOfferingName(offeringInputDTO.offeringName());
        offering.setDescription(offeringInputDTO.description());
        offering.setPrice(offeringInputDTO.price());
        offering.setDuration(offeringInputDTO.duration());

        Currency currency = Currency.valueOf(offeringInputDTO.currency());
        offering.setCurrency(currency);

        return offeringRepository.saveAndFlush(offering);
    }

    public String getProviderEmailByOfferingId(Long offeringId) {
        return offeringRepository.getProviderEmailByOfferingId(offeringId)
                .orElseThrow(() -> new EntityNotFoundException("Customer", "No Offering found with provider id: " + offeringId));
    }

}
