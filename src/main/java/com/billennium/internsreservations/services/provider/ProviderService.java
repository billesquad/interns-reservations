package com.billennium.internsreservations.services.provider;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.user.User;
import com.billennium.internsreservations.models.user.enums.Status;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.provider.dtos.ProviderInputDTO;
import com.billennium.internsreservations.repositories.ProviderRepository;
import com.billennium.internsreservations.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProviderService {

    private final ProviderRepository providerRepository;
    private final UserRepository userRepository;

    public Provider getProviderById(Long providerId) {
        return providerRepository.findById(providerId)
                .orElseThrow(() -> new EntityNotFoundException("Provider", "No provider found with id: " + providerId));
    }
    public Provider getProviderByOfferingId(Long offeringId) {
        return providerRepository.getProviderByOfferingId(offeringId)
                .orElseThrow(() -> new EntityNotFoundException("Provider", "No provider found with offering id: " + offeringId));
    }

    public Provider createProvider(Address address, ProviderInputDTO providerInputDTO, User user) {
        Provider provider = new Provider(
                address,
                providerInputDTO.getFirstName(),
                providerInputDTO.getLastName(),
                providerInputDTO.getBirthDate(),
                user.getEmail(),
                providerInputDTO.getPhoneNumber(),
                user.getAccessToken(),
                user.getRefreshToken(),
                user.getIdToken(),
                providerInputDTO.getSpecialization());
        userRepository.delete(user);
        return providerRepository.saveAndFlush(provider);
    }

    public void deleteProviderById(Long providerId) {
        Provider provider = getProviderById(providerId);
        provider.setStatus(Status.INACTIVE);
        providerRepository.saveAndFlush(provider);
    }

    public Provider updateProviderById(Long providerId, ProviderInputDTO providerInputDTO) {
        Provider provider = getProviderById(providerId);

        provider.setFirstName(providerInputDTO.getFirstName());
        provider.setLastName(providerInputDTO.getLastName());
        provider.setBirthDate(providerInputDTO.getBirthDate());
        provider.setPhoneNumber(providerInputDTO.getPhoneNumber());
        provider.setSpecialization(providerInputDTO.getSpecialization());

        return providerRepository.saveAndFlush(provider);
    }

}
