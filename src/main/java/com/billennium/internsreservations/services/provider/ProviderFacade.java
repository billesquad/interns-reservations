package com.billennium.internsreservations.services.provider;

import com.billennium.internsreservations.exceptions.user.UserAlreadyExistsException;
import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.customer.dtos.CustomerMapper;
import com.billennium.internsreservations.models.customer.dtos.CustomerRequestDTO;
import com.billennium.internsreservations.models.customer.dtos.CustomerResponseDTO;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.provider.dtos.ProviderInputDTO;
import com.billennium.internsreservations.models.provider.dtos.ProviderResponseDTO;
import com.billennium.internsreservations.models.user.User;
import com.billennium.internsreservations.models.user.enums.Status;
import com.billennium.internsreservations.services.address.AddressService;
import com.billennium.internsreservations.services.customer.CustomerService;
import com.billennium.internsreservations.services.user.UserService;
import com.billennium.internsreservations.services.workhour.WorkHourService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.billennium.internsreservations.models.provider.dtos.ProviderMapper.mapToProviderResponseDTO;

@Component
@RequiredArgsConstructor
public class ProviderFacade {

    private final ProviderService providerService;
    private final UserService userService;
    private final AddressService addressService;
    private final WorkHourService workHourService;
    private final CustomerService customerService;

    public ProviderResponseDTO createProvider(ProviderInputDTO providerInputDTO, String email) {
        User user = userService.getUserByEmail(email);
        if (user.getStatus() == Status.REGISTRATION_INCOMPLETE) {
            Address emptyAddress = addressService.createEmptyAddress();
            Provider provider = providerService.createProvider(emptyAddress, providerInputDTO, user);
            workHourService.createEmptyCalendar(provider);

            return mapToProviderResponseDTO(provider);
        }
        throw new UserAlreadyExistsException("email", "User with this email already exists.");
    }

    public void deleteProviderById(Long providerId) {
        providerService.deleteProviderById(providerId);
    }

    public ProviderResponseDTO updateProviderById(Long id, ProviderInputDTO providerInputDTO) {
        Provider provider = providerService.updateProviderById(id, providerInputDTO);
        return mapToProviderResponseDTO(provider);
    }

    public ProviderResponseDTO getProviderById(Long providerId) {
        return mapToProviderResponseDTO(providerService.getProviderById(providerId));
    }
    public ProviderResponseDTO getProviderByOfferingId(Long offeringId) {
        return mapToProviderResponseDTO(providerService.getProviderByOfferingId(offeringId));
    }
    public Page<CustomerResponseDTO> getCustomersByProviderId(Long providerId, CustomerRequestDTO customerRequestDTO) {
        return customerService.getAllCustomersIdByProvider(providerId, customerRequestDTO).map(CustomerMapper::mapToCustomerResponseDTO);
    }

}
