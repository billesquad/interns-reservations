package com.billennium.internsreservations.services.address;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.address.dtos.AddressUpdateDTO;
import com.billennium.internsreservations.repositories.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    public Address createEmptyAddress() {
        return addressRepository.saveAndFlush(new Address());
    }

    public Address getAddressById(Long addressId) {
        return addressRepository.findById(addressId)
                .orElseThrow(() -> new EntityNotFoundException("Address", "No address found with id: " + addressId));
    }

    public Address updateAddress(Long addressId, AddressUpdateDTO addressUpdateDTO) {
        Address address = getAddressById(addressId);

        address.setCountry(addressUpdateDTO.country());
        address.setCity(addressUpdateDTO.city());
        address.setStreet(addressUpdateDTO.street());
        address.setZipCode(addressUpdateDTO.zipCode());
        address.setBuildingNumber(addressUpdateDTO.buildingNumber());
        address.setApartmentNumber(addressUpdateDTO.apartmentNumber());

        return addressRepository.saveAndFlush(address);
    }

    public void deleteAddressById(Long addressId) {
        Address address = getAddressById(addressId);
        address.setIsActive(false);
        addressRepository.saveAndFlush(address);
    }

}
