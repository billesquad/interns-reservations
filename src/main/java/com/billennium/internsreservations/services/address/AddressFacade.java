package com.billennium.internsreservations.services.address;

import com.billennium.internsreservations.models.address.dtos.AddressResponseDTO;
import com.billennium.internsreservations.models.address.dtos.AddressUpdateDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.billennium.internsreservations.models.address.dtos.AddressMapper.mapToAddressResponseDTO;

@Component
@RequiredArgsConstructor
public class AddressFacade {

    private final AddressService addressService;

    public AddressResponseDTO updateAddress(Long addressId, AddressUpdateDTO addressUpdateDTO) {
        return mapToAddressResponseDTO(addressService.updateAddress(addressId, addressUpdateDTO));
    }

    public AddressResponseDTO getAddressById(Long addressId) {
        return mapToAddressResponseDTO(addressService.getAddressById(addressId));
    }

    public void deleteAddress(Long addressId) {
        addressService.deleteAddressById(addressId);
    }

}
