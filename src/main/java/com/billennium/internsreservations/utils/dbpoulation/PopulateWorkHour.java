package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.workhour.WorkHour;
import com.billennium.internsreservations.repositories.WorkHourRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalTime;

import static com.billennium.internsreservations.utils.Helpers.*;

@AllArgsConstructor
@Component
public class PopulateWorkHour {
    private final WorkHourRepository workHourRepository;

    public void insertWorkHours(Provider provider){
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            if (getRandomBoolean(0.7)||dayOfWeek==DayOfWeek.MONDAY) {
                WorkHour workHour = new WorkHour();
                LocalTime startTime = getRandomLocalTime();
                workHour.setProvider(provider);
                workHour.setDayOfWeek(dayOfWeek);
                workHour.setStartTime(startTime);
                workHour.setEndTime(startTime.plusHours(getRandomNumber(2, 8)));
                workHourRepository.save(workHour);
            }
        }
    }
}
