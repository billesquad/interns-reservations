package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.repositories.ProviderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Random;

import static com.billennium.internsreservations.utils.Helpers.NO_TOKEN;

@RequiredArgsConstructor
@Component
public class PopulateProvider {

    Faker faker = new Faker();
    public final ProviderRepository providerRepository;

    public Provider insertProvider(Address address) {
        Provider provider = new Provider(
                address,
                faker.name().firstName(),
                faker.name().lastName(),
                faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.internet().emailAddress(),
                new Random().ints(9, 0, 10)
                        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString(),
                NO_TOKEN,
                NO_TOKEN,
                NO_TOKEN,
                faker.job().title());
        providerRepository.save(provider);
        return provider;
    }
}
