package com.billennium.internsreservations.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("loadData")
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final LoadDummyData loadDummyData;
    private final ApplicationContext appContext;

    @Override
    public void run(String... args) throws Exception {
        loadDummyData.loadDummyData();
        SpringApplication.exit(appContext, () -> 0);
    }
}