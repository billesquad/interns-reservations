package com.billennium.internsreservations.utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Helpers {
    public static final String NO_TOKEN = "DummyDataNoToken";
    public static LocalTime getRandomLocalTime() {
        int minSeconds = LocalTime.of(6, 0).toSecondOfDay();
        int maxSeconds = LocalTime.of(14, 0).toSecondOfDay();

        int randomSeconds = ThreadLocalRandom.current().nextInt(minSeconds, maxSeconds);

        return LocalTime.ofSecondOfDay(randomSeconds);
    }

    public static int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public static boolean getRandomBoolean(double trueProbability) {
        Random random = new Random();
        return random.nextDouble() < trueProbability;
    }
    public static LocalDateTime getNextMondayAt(LocalTime time) {
        LocalDate today = LocalDate.now();
        LocalDate nextMonday = today.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        return LocalDateTime.of(nextMonday, time);
    }
    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int randomIndex = getRandomNumber(0,clazz.getEnumConstants().length-1);
        return clazz.getEnumConstants()[randomIndex];
    }
}
