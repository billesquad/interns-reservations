package com.billennium.internsreservations.validators.offeringpageparam;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class OfferingPageParamValidator implements ConstraintValidator<OfferingPageParam, String> {

    private static final String[] ALLOWED_VALUES = {"id", "createdAt", "modifiedAt", "provider", "offeringName",
            "description", "isActive", "duration", "price", "currency"};

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isBlank()) {
            return false;
        }
        for (String allowedValue : ALLOWED_VALUES) {
            if (allowedValue.equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}