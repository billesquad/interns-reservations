package com.billennium.internsreservations.validators.offeringpageparam;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = OfferingPageParamValidator.class)
public @interface OfferingPageParam {

    String message() default "Sorting parameter must be valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

