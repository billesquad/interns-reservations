package com.billennium.internsreservations.validators.reservationpageparam;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = ReservationPageParamValidator.class)
public @interface ReservationPageParam {

    String message() default "Reservation sorting parameter must be valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
