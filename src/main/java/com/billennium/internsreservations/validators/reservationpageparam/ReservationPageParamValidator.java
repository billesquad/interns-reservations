package com.billennium.internsreservations.validators.reservationpageparam;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ReservationPageParamValidator implements ConstraintValidator<ReservationPageParam, String> {

    private static final String[] ALLOWED_VALUES = {"id", "createdAt", "modifiedAt", "reservationDate", "status"};

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isBlank()) {
            return false;
        }
        for (String allowedValue : ALLOWED_VALUES) {
            if (allowedValue.equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}
