package com.billennium.internsreservations.validators.reservationstatus;

import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.List;

public class ReservationStatusFilterValidator implements ConstraintValidator<ReservationStatusFilter, List<String>> {

    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return false;
        }
        for (String item : value) {
            boolean isValid = false;
            for (ReservationStatus reservationStatus : ReservationStatus.values()) {
                if (reservationStatus.toString().equalsIgnoreCase(item)) {
                    isValid = true;
                    break;
                }
            }
            if (!isValid) {
                return false;
            }
        }
        return true;
    }
}
