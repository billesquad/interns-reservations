package com.billennium.internsreservations.controllers.common;

import com.billennium.internsreservations.exceptions.ErrorMessage;
import com.billennium.internsreservations.models.reservation.dtos.ReservationResponseDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationTimeDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationTimesRequestDTO;
import com.billennium.internsreservations.services.reservation.ReservationFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/common/reservation")
@Tag(name = "Common Reservation Controller",
        description = "Functionalities intended for the customers and provider")
public class ReservationController {

    private final ReservationFacade reservationFacade;

    @Operation(summary = "Get reservations times by provider Id and date",
            description = "Retrieves a reservations times")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful retrieve of reservation times",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = ReservationTimeDTO.class
                                    ))
                    )),
            @ApiResponse(
                    responseCode = "204",
                    description = "No Content"
            )
    })
    @GetMapping("/times-by-day")
    public ResponseEntity<List<ReservationTimeDTO>> getReservationsByProviderAndDay(@ModelAttribute ReservationTimesRequestDTO requestDTO) {
        List<ReservationTimeDTO> times = reservationFacade.getReservationsByProviderAndDay(requestDTO.getProviderId(), requestDTO.getDate());
        return times.size() > 0 ? ResponseEntity.ok(times) : ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get reservation by id", description = "Retrieves a reservation using the given id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful retrieve of reservation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ReservationResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Reservation not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @GetMapping("/{reservationId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityCustomerOrProviderEmailAndEmailByReservationId(#reservationId))")
    public ResponseEntity<ReservationResponseDTO> getReservationById(@PathVariable Long reservationId) {
        ReservationResponseDTO reservationResponseDTO = reservationFacade.getReservationById(reservationId);
        return new ResponseEntity<>(reservationResponseDTO, HttpStatus.OK);
    }

}
