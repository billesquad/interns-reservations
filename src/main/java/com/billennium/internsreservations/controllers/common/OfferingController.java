package com.billennium.internsreservations.controllers.common;

import com.billennium.internsreservations.models.offering.dtos.OfferingRequestDTO;
import com.billennium.internsreservations.models.offering.dtos.OfferingResponseDTO;
import com.billennium.internsreservations.services.offering.OfferingFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/common/offering")
@Tag(name = "Common Offering Controller",
        description = "Controller for offering management, designed for customer and provider")
public class OfferingController {

    private final OfferingFacade offeringFacade;

    @Operation(summary = "Get offering by id", description = "Retrieves an offering using the given id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful getting offering",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OfferingResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - resource could not be found on the server",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    @GetMapping("/{offeringId}")
    public ResponseEntity<OfferingResponseDTO> getOfferingById(@PathVariable Long offeringId) {
        OfferingResponseDTO offeringResponseDTO = offeringFacade.getOfferingById(offeringId);
        return ResponseEntity.ok(offeringResponseDTO);
    }

    @GetMapping("/all")
    @Operation(summary = "Show all offerings", description = "Functionality lets user to show all available offerings")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful offering acquisition",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(implementation = OfferingResponseDTO.class)
                            )
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - temporally returns map of errors or ErrorMessage",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    public ResponseEntity<Page<OfferingResponseDTO>> getOfferings(
            @ModelAttribute @Valid OfferingRequestDTO offeringRequestDto) {
        Page<OfferingResponseDTO> offeringResponseDTOPage = offeringFacade.getOfferings(offeringRequestDto);
        return new ResponseEntity<>(offeringResponseDTOPage, HttpStatus.OK);
    }

}
