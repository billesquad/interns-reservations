package com.billennium.internsreservations.controllers.common;

import com.billennium.internsreservations.exceptions.ErrorMessage;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourResponseDTO;
import com.billennium.internsreservations.services.workhour.WorkHourFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/common/work-hour")
@Tag(name = "Common Work hour Controller",
        description = "Controller for work hours management, designed for customer and provider")
public class WorkHourController {

    private final WorkHourFacade workHourFacade;

    @Operation(summary = "Get work hour by id", description = "Retrieves a work hour using the given id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful retrieve of work hour",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = WorkHourResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Work hour not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @GetMapping("/{workHourId}")
    public ResponseEntity<WorkHourResponseDTO> getWorkHourById(@PathVariable Long workHourId) {
        WorkHourResponseDTO workHourResponseDTO = workHourFacade.getWorkHourById(workHourId);
        return new ResponseEntity<>(workHourResponseDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get work hours (calendar) by provider id",
            description = "Retrieves a work hours using the given id sorting the list by days of the week")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful retrieve of work hours",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = WorkHourResponseDTO.class
                                    ))
                    )),
            @ApiResponse(
                    responseCode = "204",
                    description = "No Content"
            )
    })
    @GetMapping("/calendar/{providerId}")
    public ResponseEntity<List<WorkHourResponseDTO>> getProviderCalendar(@PathVariable Long providerId) {
        List<WorkHourResponseDTO> workHours = workHourFacade.getProviderCalendar(providerId);
        return workHours.size() > 0 ? ResponseEntity.ok(workHours) : ResponseEntity.noContent().build();
    }

}
