package com.billennium.internsreservations.controllers.provider;

import com.billennium.internsreservations.models.reservation.dtos.ReservationRequestDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationResponseDTO;
import com.billennium.internsreservations.services.reservation.ReservationFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/provider/reservation")
@RequiredArgsConstructor
@Tag(name = "Provider Controller")
public class ProviderReservationController {

    private final ReservationFacade reservationFacade;

    @Operation(summary = "Get reservations by provider id", description = "Retrieves all reservations of provider by provider Id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Reservations obtained successfully",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = ReservationResponseDTO.class
                                    ))
                    ))
    })
    @GetMapping("/all/{providerId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityEmailAndEmailByUserId(#providerId))")
    public ResponseEntity<Page<ReservationResponseDTO>> getAllReservationsByProvider(@PathVariable Long providerId, @ModelAttribute @Valid ReservationRequestDTO reservationDTO) {
        return new
                ResponseEntity<>(reservationFacade.getProviderReservations(providerId, reservationDTO), HttpStatus.OK);
    }

    @Operation(summary = "Change reservation's status by reservation id", description = "Changes reservation's status using provider id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Reservation's status changed",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ReservationResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - resource could not be found on the server",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    @PatchMapping("/change-status/{reservationId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityProviderEmailAndEmailByReservationId(#reservationId))")
    public ResponseEntity<ReservationResponseDTO> changeReservationStatus(@PathVariable Long reservationId, @RequestParam String status) {
        ReservationResponseDTO reservationResponseDTO = reservationFacade.changeReservationStatus(reservationId, status);
        return new ResponseEntity<>(reservationResponseDTO, HttpStatus.OK);
    }

}
