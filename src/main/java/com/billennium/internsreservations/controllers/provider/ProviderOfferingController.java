package com.billennium.internsreservations.controllers.provider;

import com.billennium.internsreservations.exceptions.ErrorMessage;
import com.billennium.internsreservations.models.offering.dtos.OfferingInputDTO;
import com.billennium.internsreservations.models.offering.dtos.OfferingProviderRequestDTO;
import com.billennium.internsreservations.models.offering.dtos.OfferingResponseDTO;
import com.billennium.internsreservations.services.offering.OfferingFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/provider/offering")
@Tag(name = "Provider Controller")
public class ProviderOfferingController {

    private final OfferingFacade offeringFacade;

    @Operation(summary = "Create new offering", description = "Creates a offering from the provided payload")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Successful creation of offering",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OfferingResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - returns map of errors",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Provider not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PostMapping("")
    @PreAuthorize("(@fineGrainServices.getCurrentUserId()==#offeringInputDTO.providerId())")
    public ResponseEntity<OfferingResponseDTO> createOffering(@RequestBody @Valid OfferingInputDTO offeringInputDTO) {
        OfferingResponseDTO offeringResponseDTO = offeringFacade.createOffering(offeringInputDTO);
        return new ResponseEntity<>(offeringResponseDTO, HttpStatus.CREATED);
    }

    @Operation(summary = "Delete offering by id", description = "Deletes offering by change isActive status to false")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "No Content - Successful deletion offering"
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - offering not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PatchMapping("/delete/{id}")
    @PreAuthorize("(@fineGrainServices.compareSecurityEmailAndEmailByOfferingId(#id))")
    public ResponseEntity<?> deleteOfferingById(@PathVariable Long id) {
        offeringFacade.deleteOfferingById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Update an offering", description = "Updates an offering from the provided payload")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Offering's update successful",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OfferingResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - returns map of errors",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Offering not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PutMapping("/update/{offeringId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityEmailAndEmailByOfferingId(#offeringId))")
    public ResponseEntity<OfferingResponseDTO> updateOfferingById(@PathVariable Long offeringId,
                                                                  @Valid @RequestBody OfferingInputDTO offeringInputDTO) {
        OfferingResponseDTO updatedOffering = offeringFacade.updateOfferingById(offeringId, offeringInputDTO);
        return ResponseEntity.ok(updatedOffering);
    }
    @Operation(summary = "Get all provider's offerings", description = "Gets all offerings  with matching provider's id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful offering acquisition",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(implementation = OfferingResponseDTO.class)
                            )
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - temporally returns map of errors or ErrorMessage",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    @GetMapping("/get-by-provider/{providerId}")
    @PreAuthorize("(@fineGrainServices.getCurrentUserId()==#providerId)")
    public ResponseEntity<Page<OfferingResponseDTO>> getOfferingsByProvider(@PathVariable Long providerId,
                                                                            @ModelAttribute @Valid OfferingProviderRequestDTO offeringProviderRequestDTO) {
        Page<OfferingResponseDTO> offeringResponseDTOPage = offeringFacade.getOfferingsByProviderId(providerId, offeringProviderRequestDTO);
        return ResponseEntity.ok(offeringResponseDTOPage);
    }

}
