package com.billennium.internsreservations.controllers.provider;

import com.billennium.internsreservations.exceptions.ErrorMessage;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourResponseDTO;
import com.billennium.internsreservations.models.workhour.dtos.WorkHourUpdateDTO;
import com.billennium.internsreservations.services.workhour.WorkHourFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/provider/work-hour")
@Tag(name = "Provider Controller")
public class ProviderWorkHourController {

    private final WorkHourFacade workHourFacade;

    @Operation(summary = "Update work hours", description = "Updates a work hours with the provided payload")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful update of work hours",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = WorkHourResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - temporally returns map of errors or ErrorMessage",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - when no work hour is found with the provided id",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PatchMapping("/update/{workHourId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityEmailAndEmailByWorkHourId(#workHourId))")
    public ResponseEntity<WorkHourResponseDTO> updateWorkHour(@PathVariable Long workHourId,
                                                              @RequestBody @Valid WorkHourUpdateDTO workHourUpdateDTO) {
        WorkHourResponseDTO workHourResponseDTO = workHourFacade.updateWorkHour(workHourId, workHourUpdateDTO);
        return new ResponseEntity<>(workHourResponseDTO, HttpStatus.OK);
    }

    @Operation(summary = "Delete work hour",
            description = "Deletes a work hour with the given id - changes start time and end time to nulls")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "No Content - Successful deletion of work hour"
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Work hour not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PatchMapping("/delete/{workHourId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityEmailAndEmailByWorkHourId(#workHourId))")
    public ResponseEntity<Void> deleteWorkHour(@PathVariable Long workHourId) {
        workHourFacade.deleteWorkHour(workHourId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
