package com.billennium.internsreservations.controllers.customer;

import com.billennium.internsreservations.exceptions.ErrorMessage;
import com.billennium.internsreservations.models.customer.dtos.CustomerResponseDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationCreateDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationCustomerRequestDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationResponseDTO;
import com.billennium.internsreservations.services.reservation.ReservationFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/customer/reservation")
@Tag(name = "Customer Controller")
public class CustomerReservationController {

    private final ReservationFacade reservationFacade;

    @Operation(summary = "Create new reservation", description = "Creates a reservation from the provided payload")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Successful creation of reservation.",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ReservationResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - temporally returns map of errors or ErrorMessage.",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Offering/Provider/Customer not found - returns ErrorMessage",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessage.class)
                    ))
    })
    @PostMapping("")
    @PreAuthorize("@fineGrainServices.getCurrentUserId()==#reservationCreateDTO.customerId()")
    public ResponseEntity<ReservationResponseDTO> createReservation(@RequestBody @Valid ReservationCreateDTO reservationCreateDTO) {
        ReservationResponseDTO reservationResponseDTO = reservationFacade.createReservation(reservationCreateDTO);
        return new ResponseEntity<>(reservationResponseDTO, HttpStatus.CREATED);
    }

    @Operation(summary = "Cancel reservation", description = "Cancel reservation by reservation Id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Reservation cancelled successfully",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ReservationResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found - Reservation with provided id does not exist.",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    @PatchMapping(value = "/cancel/{reservationId}")
    @PreAuthorize("(@fineGrainServices.compareSecurityCustomerEmailAndEmailByReservationId(#reservationId))")
    public ResponseEntity<ReservationResponseDTO> cancelReservation(@PathVariable Long reservationId) {
        return new ResponseEntity<>(reservationFacade.cancelReservationById(reservationId), HttpStatus.OK);
    }

    @Operation(summary = "Show all reservations by customer Id", description = "This feature enables users to display all of their reservations")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successful offering acquisition",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CustomerResponseDTO.class)
                    )),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request - temporally returns map of errors or ErrorMessage",
                    content = @Content(
                            mediaType = "application/json"
                    )),
    })
    @GetMapping("/all/{customerId}")
    @PreAuthorize("@fineGrainServices.compareSecurityEmailAndEmailByUserId(#customerId)")
    public ResponseEntity<Page<ReservationResponseDTO>> getCustomerReservations(
            @PathVariable Long customerId,
            @ModelAttribute @Valid ReservationCustomerRequestDTO reservationCustomerRequestDto) {

        Page<ReservationResponseDTO> reservationResponseDTOPage = reservationFacade.getCustomerReservations(customerId, reservationCustomerRequestDto);
        return new ResponseEntity<>(reservationResponseDTOPage, HttpStatus.OK);
    }

}