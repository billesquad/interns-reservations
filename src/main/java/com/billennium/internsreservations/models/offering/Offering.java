package com.billennium.internsreservations.models.offering;

import com.billennium.internsreservations.models.basic.BasicEntity;
import com.billennium.internsreservations.models.offering.enums.Currency;
import com.billennium.internsreservations.models.provider.Provider;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name = "offerings")
@Setter
@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Offering extends BasicEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    Provider provider;

    String offeringName;
    String description;
    Boolean isActive;
    Integer duration;
    Double price;

    @Enumerated(EnumType.STRING)
    Currency currency;

    public Offering(Provider provider, String offeringName, String description, Integer duration, Double price, Currency currency) {
        this.provider = provider;
        this.offeringName = offeringName;
        this.description = description;
        this.isActive = true;
        this.duration = duration;
        this.price = price;
        this.currency = currency;
    }

}
