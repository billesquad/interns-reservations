package com.billennium.internsreservations.models.offering.dtos;

import lombok.Builder;

public record OfferingResponseDTO(Long offeringId,
                                  Long providerId,
                                  String providerFullName,
                                  String providerSpecialization,
                                  String offeringName,
                                  Integer duration,
                                  Double price,
                                  String currency,
                                  String description) {

    @Builder
    public OfferingResponseDTO {}

}
