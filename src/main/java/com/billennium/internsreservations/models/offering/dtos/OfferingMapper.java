package com.billennium.internsreservations.models.offering.dtos;

import com.billennium.internsreservations.models.offering.Offering;

public class OfferingMapper {

    public static OfferingResponseDTO mapToOfferingResponseDTO(Offering offering) {
        return OfferingResponseDTO
                .builder()
                .offeringId(offering.getId())
                .providerId(offering.getProvider().getId())
                .providerFullName(offering.getProvider().getFirstName() + " " + offering.getProvider().getLastName())
                .providerSpecialization(offering.getProvider().getSpecialization())
                .offeringName(offering.getOfferingName())
                .duration(offering.getDuration())
                .price(offering.getPrice())
                .currency(offering.getCurrency().name())
                .description(offering.getDescription() == null ? "" : offering.getDescription())
                .build();
    }

}
