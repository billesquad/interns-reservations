package com.billennium.internsreservations.models.offering.dtos;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public record OfferingInputDTO(@NotNull(message = "Provider id cannot be empty.")
                               Long providerId,
                               @NotBlank(message = "Offering name field must be filled in.")
                               @Length(min = 3, message = "Offering name must be at least 8 characters.")
                               String offeringName,
                               String description,
                               @NotNull(message = "Duration cannot be empty.")
                               @Min(value = 0, message = "Duration must not be less than or equal to zero.")
                               Integer duration,
                               @NotNull(message = "Price cannot be empty.")
                               @Min(value = 0, message = "Price must not be less than or equal to zero.")
                               Double price,
                               @NotNull(message = "Currency cannot be empty.")
                               String currency) {
}
