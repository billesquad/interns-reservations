package com.billennium.internsreservations.models.offering.dtos;

import com.billennium.internsreservations.validators.offeringpageparam.OfferingPageParam;
import com.billennium.internsreservations.validators.pagedirection.PageDirection;
import com.billennium.internsreservations.validators.pagesize.PageSize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Schema
@NoArgsConstructor
public class OfferingRequestDTO {

    @Schema(example = "0")
    @Min(value = 0)
    private String page = "0";
    @Schema(example = "25")
    @PageSize
    private String size = "10";
    @Schema(example = "price")
    @OfferingPageParam
    private String sortParam = "price";
    @Schema(example = "DESC")
    @PageDirection
    private String direction = "ASC";
    @Schema(example = "O")
    private String nameSearch = null;
    @Schema(example = "0")
    @Min(value = 0, message = "Minimum price must not be less than or equal to zero.")
    private String priceFrom = null;
    @Schema(example = "9999")
    @Min(value = 1, message = "Maximum price must not be less than or equal to one.")
    private String priceTo = null;
    @Schema(example = "Smith")
    private String provider = null;

}