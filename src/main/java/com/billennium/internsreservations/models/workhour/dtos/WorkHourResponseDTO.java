package com.billennium.internsreservations.models.workhour.dtos;

import lombok.Builder;

import java.time.LocalTime;

public record WorkHourResponseDTO(Long workHourId,
                                  Long providerId,
                                  String dayOfWeek,
                                  LocalTime startTime,
                                  LocalTime endTime
) {

    @Builder
    public WorkHourResponseDTO {}
}
