package com.billennium.internsreservations.models.workhour.dtos;

import com.billennium.internsreservations.models.workhour.WorkHour;

import java.time.format.TextStyle;
import java.util.Locale;

public class WorkHourMapper {

    public static WorkHourResponseDTO mapToWorkHourResponseDTO(WorkHour workHour) {
        return WorkHourResponseDTO.builder()
                .workHourId(workHour.getId())
                .providerId(workHour.getProvider().getId())
                .dayOfWeek(workHour.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US))
                .startTime(workHour.getStartTime())
                .endTime(workHour.getEndTime())
                .build();
    }
}
