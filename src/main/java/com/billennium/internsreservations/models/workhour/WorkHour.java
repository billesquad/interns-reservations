package com.billennium.internsreservations.models.workhour;

import com.billennium.internsreservations.models.basic.BasicEntity;
import com.billennium.internsreservations.models.provider.Provider;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "work_hours")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkHour extends BasicEntity {

    public static Integer MINIMAL_DURATION = 1;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    Provider provider;

    @Enumerated(EnumType.STRING)
    DayOfWeek dayOfWeek;

    LocalTime startTime;
    LocalTime endTime;

}
