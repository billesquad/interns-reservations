package com.billennium.internsreservations.models.workhour.dtos;

import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record WorkHourUpdateDTO(@NotNull(message = "Start time field must be filled in.")
                                LocalTime startTime,
                                @NotNull(message = "End time field must be filled in.")
                                LocalTime endTime) {
}
