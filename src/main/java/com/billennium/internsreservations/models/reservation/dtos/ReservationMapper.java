package com.billennium.internsreservations.models.reservation.dtos;

import com.billennium.internsreservations.models.reservation.Reservation;

public class ReservationMapper {

    public static ReservationResponseDTO toReservationDTO(Reservation reservation) {
        return ReservationResponseDTO
                .builder()
                .reservationId(reservation.getId())
                .customerId(reservation.getCustomer().getId())
                .offeringId(reservation.getOffering().getId())
                .providerId(reservation.getOffering().getProvider().getId())
                .offeringName(reservation.getOffering().getOfferingName())
                .price(reservation.getOffering().getPrice())
                .currency(reservation.getOffering().getCurrency().name())
                .description(reservation.getOffering().getDescription())
                .providerFullName(reservation.getOffering().getProvider().getFirstName()
                        + " " + reservation.getOffering().getProvider().getLastName())
                .providerSpecialization(reservation.getOffering().getProvider().getSpecialization())
                .customerFullName(reservation.getCustomer().getFirstName()
                + " " + reservation.getCustomer().getLastName())
                .reservationDate(reservation.getReservationDate())
                .reservationDateEnd(reservation.getReservationDateEnd())
                .status((reservation.getStatus()).toString())
                .build();
    }

    public static ReservationTimeDTO toReservationTimeDTO(Reservation reservation) {
        return ReservationTimeDTO
                .builder()
                .reservationStartTime(reservation.getReservationDate().toLocalTime())
                .reservationEndTime(reservation.getReservationDateEnd().toLocalTime())
                .build();
    }
}
