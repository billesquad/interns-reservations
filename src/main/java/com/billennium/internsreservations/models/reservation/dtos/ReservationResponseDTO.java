package com.billennium.internsreservations.models.reservation.dtos;

import lombok.Builder;

import java.time.LocalDateTime;

public record ReservationResponseDTO(Long reservationId,
                                     Long customerId,
                                     Long offeringId,
                                     Long providerId,
                                     String offeringName,
                                     Double price,
                                     String currency,
                                     String description,
                                     String providerFullName,
                                     String providerSpecialization,
                                     String customerFullName,
                                     LocalDateTime reservationDate,
                                     LocalDateTime reservationDateEnd,
                                     String status) {
    @Builder
    public ReservationResponseDTO {}

}
