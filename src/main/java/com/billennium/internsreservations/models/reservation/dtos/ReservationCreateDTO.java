package com.billennium.internsreservations.models.reservation.dtos;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

public record ReservationCreateDTO(@NotNull(message = "Customer ID cannot be null.")
                                   @Min(value = 0, message = "Customer ID must be a positive number.")
                                   Long customerId,
                                   @NotNull(message = "Offering ID cannot be null.")
                                   @Min(value = 0, message = "Offering ID must be a positive number.")
                                   Long offeringId,
                                   @Future(message = "Reservation date must be in the future")
                                   LocalDateTime reservationStartDate) {
}
