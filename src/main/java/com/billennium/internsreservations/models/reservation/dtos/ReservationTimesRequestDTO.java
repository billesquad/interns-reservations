package com.billennium.internsreservations.models.reservation.dtos;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ReservationTimesRequestDTO {
    @NotNull
    private Long providerId;
    @NotNull
    private LocalDate date;
}
