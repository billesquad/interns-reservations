package com.billennium.internsreservations.models.reservation.enums;

import java.util.Arrays;

public enum ReservationStatus {
    AWAITING_FOR_APPROVAL,
    CANCELLED,
    ACCEPTED,
    DECLINED,
    DONE;

    public static boolean isValid(String value) {
        return Arrays.stream(ReservationStatus.values())
                .anyMatch(reservationStatus -> reservationStatus.name().equals(value));
    }

}
