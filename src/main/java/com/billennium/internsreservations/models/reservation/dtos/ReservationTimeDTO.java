package com.billennium.internsreservations.models.reservation.dtos;

import lombok.Builder;

import java.time.LocalTime;

public record ReservationTimeDTO (LocalTime reservationStartTime,
                                 LocalTime reservationEndTime) {

    @Builder
    public ReservationTimeDTO {}

}
