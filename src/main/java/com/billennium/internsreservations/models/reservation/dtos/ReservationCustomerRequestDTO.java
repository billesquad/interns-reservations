package com.billennium.internsreservations.models.reservation.dtos;

import com.billennium.internsreservations.validators.pagedirection.PageDirection;
import com.billennium.internsreservations.validators.pagesize.PageSize;
import com.billennium.internsreservations.validators.reservationpageparam.ReservationPageParam;
import com.billennium.internsreservations.validators.reservationstatus.ReservationStatusFilter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Schema
@NoArgsConstructor
public class ReservationCustomerRequestDTO {

    @Schema(example = "0")
    @Min(value = 0)
    private String page = "0";
    @Schema(example = "25")
    @PageSize
    private String size = "10";
    @Schema(example = "reservationDate")
    @ReservationPageParam
    private String sortParam = "reservationDate";
    @Schema(example = "DESC")
    @PageDirection
    private String direction = "DESC";
    @Schema(example = "AWAITING_FOR_APPROVAL")
    @ReservationStatusFilter
    private List<String> statuses = List.of("DONE","DECLINED","ACCEPTED","CANCELLED","AWAITING_FOR_APPROVAL");

}
