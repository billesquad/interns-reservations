package com.billennium.internsreservations.models.reservation;


import com.billennium.internsreservations.models.basic.BasicEntity;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "reservations")
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Reservation extends BasicEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offering_id", referencedColumnName = "id")
    Offering offering;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    Customer customer;

    LocalDateTime reservationDate;
    LocalDateTime reservationDateEnd;

    @Enumerated(EnumType.STRING)
    ReservationStatus status;

    public Reservation(Offering offering, Customer customer, LocalDateTime reservationDate,
                       LocalDateTime reservationDateEnd) {
        this.offering = offering;
        this.customer = customer;
        this.reservationDate = reservationDate;
        this.reservationDateEnd = reservationDateEnd;
        this.status = ReservationStatus.AWAITING_FOR_APPROVAL;
    }

}

