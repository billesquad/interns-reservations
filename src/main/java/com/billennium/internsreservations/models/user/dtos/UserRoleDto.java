package com.billennium.internsreservations.models.user.dtos;

public record UserRoleDto(String role, Long userId) {
}
