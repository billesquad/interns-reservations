package com.billennium.internsreservations.models.user.enums;

public enum Status {
    REGISTRATION_INCOMPLETE,
    ACTIVE,
    INACTIVE

}
