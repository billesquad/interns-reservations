package com.billennium.internsreservations.models.customer.dtos;

import com.billennium.internsreservations.models.user.enums.Status;
import lombok.Builder;

import java.time.LocalDate;

public record CustomerResponseDTO(Long customerId,
                                  Long addressId,
                                  Status status,
                                  String firstName,
                                  String lastName,
                                  LocalDate birthDate,
                                  String email,
                                  String phoneNumber) {

    @Builder
    public CustomerResponseDTO {}

}
