package com.billennium.internsreservations.models.customer;

import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.user.User;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "customers")
@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Customer extends User {

    public Customer(Address address,
                    String firstName,
                    String lastName,
                    LocalDate birthDate,
                    String email,
                    String phoneNumber,
                    String accessToken,
                    String refreshToken,
                    String idToken) {
        super(address, firstName, lastName, birthDate, email, phoneNumber, accessToken, refreshToken, idToken);
    }

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    List<Reservation> reservations;

}
