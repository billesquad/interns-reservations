package com.billennium.internsreservations.models.customer.dtos;

import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.user.enums.Status;

public class CustomerMapper {

    public static CustomerResponseDTO mapToCustomerResponseDTO(Customer customer) {
        return CustomerResponseDTO.builder()
                .customerId(customer.getId())
                .addressId(customer.getAddress().getId())
                .status(Status.valueOf(customer.getStatus().name()))
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .birthDate(customer.getBirthDate())
                .email(customer.getEmail())
                .phoneNumber(customer.getPhoneNumber())
                .build();
    }

}
