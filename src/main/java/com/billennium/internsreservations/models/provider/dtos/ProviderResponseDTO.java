package com.billennium.internsreservations.models.provider.dtos;

import com.billennium.internsreservations.models.user.enums.Status;
import lombok.Builder;

import java.time.LocalDate;

public record ProviderResponseDTO(Long providerId,
                                  Long addressId,
                                  Status status,
                                  String firstName,
                                  String lastName,
                                  LocalDate birthDate,
                                  String email,
                                  String phoneNumber,
                                  String specialization) {

    @Builder
    public ProviderResponseDTO {}

}
