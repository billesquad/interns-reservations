package com.billennium.internsreservations.models.provider.dtos;

import com.billennium.internsreservations.models.user.dtos.UserInputDTO;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

@Getter
public class ProviderInputDTO extends UserInputDTO {

    @NotBlank(message = "Specialization field must be filled in.")
    @Length(min = 3, message = "Specialization must be at least 3 characters.")
    private String specialization;

}
