package com.billennium.internsreservations.models.provider.dtos;

import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.user.enums.Status;

public class ProviderMapper {

    public static ProviderResponseDTO mapToProviderResponseDTO(Provider provider) {
        return ProviderResponseDTO.builder()
                .providerId(provider.getId())
                .addressId(provider.getAddress().getId())
                .status(Status.valueOf(provider.getStatus().name()))
                .firstName(provider.getFirstName())
                .lastName(provider.getLastName())
                .birthDate(provider.getBirthDate())
                .email(provider.getEmail())
                .phoneNumber(provider.getPhoneNumber())
                .specialization(provider.getSpecialization())
                .build();
    }

}
