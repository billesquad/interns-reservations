package com.billennium.internsreservations.services.customer;

import com.billennium.internsreservations.exceptions.EntityNotFoundException;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.repositories.CustomerRepository;
import com.billennium.internsreservations.utils.LoadDummyData;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetCustomerIntegrationTest {
    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    LoadDummyData loadDummyData;

    @BeforeAll
    public void runLoadDummyData(){
        loadDummyData.loadDummyData();
    }
    @AfterAll
    public static void cleanUpDatabase() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:myDb", "sa", "");
        Statement stmt = conn.createStatement();
        stmt.execute("DELETE FROM reservations");
        stmt.execute("DELETE FROM offerings");
        stmt.execute("DELETE FROM customers");
        stmt.execute("DELETE FROM work_hours");
        stmt.execute("DELETE FROM providers");
        stmt.execute("DELETE FROM users");
        stmt.execute("DELETE FROM addresses");
        stmt.close();
        conn.close();
    }

    @Test
    public void testGetCustomer(){
        Long idOfValidCustomer = customerRepository.findAll().get(0).getId();
        Customer customer = customerService.getCustomerById(idOfValidCustomer);
        assertEquals(idOfValidCustomer, customer.getId());
    }
    @Test
    public void testGetCustomerInvalidId() {
        Long nonExistentCustomerId = -1L;
        assertThrows(EntityNotFoundException.class, () -> {
            customerService.getCustomerById(nonExistentCustomerId);
        });
    }
}
