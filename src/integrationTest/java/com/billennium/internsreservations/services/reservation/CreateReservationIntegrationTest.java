package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.models.reservation.dtos.ReservationCreateDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationResponseDTO;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import com.billennium.internsreservations.models.workhour.WorkHour;
import com.billennium.internsreservations.repositories.CustomerRepository;
import com.billennium.internsreservations.repositories.OfferingRepository;
import com.billennium.internsreservations.repositories.ProviderRepository;
import com.billennium.internsreservations.repositories.WorkHourRepository;
import com.billennium.internsreservations.services.customer.CustomerService;
import com.billennium.internsreservations.utils.MakeReservationIntegrationTestSetUp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

import static com.billennium.internsreservations.utils.Helpers.getNextMondayAt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateReservationIntegrationTest {
    @Autowired
    CustomerService customerService;
    @Autowired
    ReservationService reservationService;
    @Autowired
    ReservationFacade reservationFacade;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    OfferingRepository offeringRepository;
    @Autowired
    WorkHourRepository workHourRepository;
    @Autowired
    MakeReservationIntegrationTestSetUp makeReservationIntegrationTestSetUp;

    @BeforeEach
    public void runLoadDummyData(){
        makeReservationIntegrationTestSetUp.loadDummyData();
    }
    @AfterEach
    public void cleanUpDatabase() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:myDb", "sa", "");
        Statement stmt = conn.createStatement();
        stmt.execute("DELETE FROM reservations");
        stmt.execute("DELETE FROM offerings");
        stmt.execute("DELETE FROM customers");
        stmt.execute("DELETE FROM work_hours");
        stmt.execute("DELETE FROM providers");
        stmt.execute("DELETE FROM users");
        stmt.execute("DELETE FROM addresses");
        stmt.close();
        conn.close();
    }

    private void successfulCreation(ReservationResponseDTO reservationResponseDTO, ReservationCreateDTO reservationCreateDTO){
        assertEquals(reservationResponseDTO.reservationDate().getDayOfWeek(), DayOfWeek.MONDAY);
        assertEquals(reservationCreateDTO.reservationStartDate().getDayOfWeek(), DayOfWeek.MONDAY);
        assertEquals(reservationResponseDTO.reservationDate().toLocalDate(), reservationResponseDTO.reservationDateEnd().toLocalDate());
        assertEquals(reservationResponseDTO.offeringId(), reservationCreateDTO.offeringId());
        assertEquals(reservationResponseDTO.customerId(), reservationCreateDTO.customerId());
        assertEquals(reservationResponseDTO.status(), ReservationStatus.AWAITING_FOR_APPROVAL.toString());
    }

    @Test
    public void testReservationCreateValid(){
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offeringRepository.findAll().get(0).getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalTime startTime = workHour.getStartTime();
        LocalDate today = LocalDate.now();
        LocalDate nextMonday = today.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        ReservationCreateDTO testDTo = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                LocalDateTime.of(nextMonday, (startTime.plusMinutes(1)))
        );
        ReservationResponseDTO reservationResponseDTO = reservationFacade.createReservation(testDTo);

        successfulCreation(reservationResponseDTO, testDTo);
    }

    @Test
    public void testReservationCreateInvalidWorkHoursBefore() {
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offeringRepository.findAll().get(0).getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalTime startTime = workHour.getStartTime();
        LocalDate today = LocalDate.now();
        LocalDate nextMonday = today.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        ReservationCreateDTO testDTo = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                LocalDateTime.of(nextMonday, (startTime.minusMinutes(1)))
        );
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationResponseDTO = reservationFacade.createReservation(testDTo);
        });
    }

    @Test
    public void testReservationCreateInvalidWorkHoursAfter() {
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offeringRepository.findAll().get(0).getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalTime startTime = workHour.getEndTime();
        LocalDate today = LocalDate.now();
        LocalDate nextMonday = today.with(TemporalAdjusters.next(DayOfWeek.MONDAY));

        ReservationCreateDTO testDTo = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                LocalDateTime.of(nextMonday, (startTime.plusMinutes(1)))
        );
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationResponseDTO = reservationFacade.createReservation(testDTo);
        });
    }

    @Test
    public void testCollisionMid() {
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offering.getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalDateTime reservationDate = getNextMondayAt(workHour.getStartTime());
        ReservationCreateDTO testDTo = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                reservationDate
        );
        reservationFacade.createReservation(testDTo);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationResponseDTO = reservationFacade.createReservation(testDTo);
        });
    }

    @Test
    public void testCollisionEnd() {
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offering.getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalDateTime reservationDate = getNextMondayAt(workHour.getStartTime());
        ReservationCreateDTO dtoFirst = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                reservationDate
        );
        ReservationResponseDTO reservationFirst = reservationFacade.createReservation(dtoFirst);

        LocalDateTime newReservationDate = reservationFirst.reservationDate().minusMinutes(1);
        ReservationCreateDTO dtoSecond = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                newReservationDate
        );
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationSecond = reservationFacade.createReservation(dtoSecond);
        });
    }

    @Test
    public void testCollisionStart() {
        Offering offering = offeringRepository.findAll().get(0);
        Provider offeringProvider = offering.getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalDateTime reservationDate = getNextMondayAt(workHour.getStartTime());
        ReservationCreateDTO dtoFirst = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                reservationDate
        );
        ReservationResponseDTO reservationFirst = reservationFacade.createReservation(dtoFirst);

        LocalDateTime newReservationDate = reservationFirst.reservationDateEnd().minusMinutes(1);
        ReservationCreateDTO dtoSecond = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                newReservationDate
        );
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationSecond = reservationFacade.createReservation(dtoSecond);
        });
    }

    @Test
    public void sameDateDifferentProviders() {
        List<Offering> allOfferings = offeringRepository.findAll();
        Offering offering = allOfferings.get(0);
        Offering offering1 = allOfferings.get(2);
        Provider offeringProvider = offering.getProvider();
        Provider offeringProvider2 = offering1.getProvider();

        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        WorkHour workHour2 = workHourRepository.getProviderCalendar(offeringProvider2.getId()).get(0);
        workHour2.setStartTime(workHour.getStartTime());
        workHourRepository.save(workHour2);

        assertEquals(workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0).getStartTime(), workHourRepository.getProviderCalendar(offeringProvider2.getId()).get(0).getStartTime());

        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);
        assertEquals(workHour2.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalDateTime reservationDate = getNextMondayAt(workHour.getStartTime());
        LocalDateTime reservationDate1 = getNextMondayAt(workHour2.getStartTime());
        ReservationCreateDTO dtoFirst = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                reservationDate
        );
        ReservationResponseDTO reservationFirst = reservationFacade.createReservation(dtoFirst);
        successfulCreation(reservationFirst, dtoFirst);

        ReservationCreateDTO dtoSecond = new ReservationCreateDTO(
                customer.getId(),
                offering1.getId(),
                reservationDate1
        );
        ReservationResponseDTO reservationSecond = reservationFacade.createReservation(dtoSecond);
        successfulCreation(reservationSecond, dtoSecond);

        assertEquals(reservationFirst.reservationDate(), reservationSecond.reservationDate());
    }

    @Test
    public void sameConflictProviderDifferentOfferings() {
        Offering offering = offeringRepository.findAll().get(0);
        Offering offering1 = offeringRepository.findAll().get(1);
        Provider offeringProvider = offering.getProvider();
        WorkHour workHour = workHourRepository.getProviderCalendar(offeringProvider.getId()).get(0);
        Customer customer = customerRepository.findAll().get(0);

        assertEquals(workHour.getDayOfWeek(), DayOfWeek.MONDAY);

        LocalDateTime reservationDate = getNextMondayAt(workHour.getStartTime());
        ReservationCreateDTO dtoFirst = new ReservationCreateDTO(
                customer.getId(),
                offering.getId(),
                reservationDate
        );
        ReservationResponseDTO reservationFirst = reservationFacade.createReservation(dtoFirst);

        ReservationCreateDTO dtoSecond = new ReservationCreateDTO(
                customer.getId(),
                offering1.getId(),
                reservationDate
        );

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            ReservationResponseDTO reservationSecond = reservationFacade.createReservation(dtoSecond);
        });
    }

}