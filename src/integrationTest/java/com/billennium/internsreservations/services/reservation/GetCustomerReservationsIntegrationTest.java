package com.billennium.internsreservations.services.reservation;

import com.billennium.internsreservations.models.reservation.dtos.ReservationCustomerRequestDTO;
import com.billennium.internsreservations.models.reservation.dtos.ReservationResponseDTO;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import com.billennium.internsreservations.repositories.CustomerRepository;
import com.billennium.internsreservations.utils.MakeReservationIntegrationTestSetUp;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest

public class GetCustomerReservationsIntegrationTest {
    
    @Autowired
    MakeReservationIntegrationTestSetUp makeReservationIntegrationTestSetUp;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ReservationFacade reservationFacade;

    @BeforeEach
    public void runLoadDummyData(){
        makeReservationIntegrationTestSetUp.loadDummyData();
    }

    @AfterAll
    public static void cleanUpDatabase() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:myDb", "sa", "");
        Statement stmt = conn.createStatement();
        stmt.execute("DELETE FROM reservations");
        stmt.execute("DELETE FROM offerings");
        stmt.execute("DELETE FROM customers");
        stmt.execute("DELETE FROM work_hours");
        stmt.execute("DELETE FROM providers");
        stmt.execute("DELETE FROM users");
        stmt.execute("DELETE FROM addresses");
        stmt.close();
        conn.close();
    }
    @Test
    public void getReservationsBasicTest() {
        Long customerId = customerRepository.findAll().get(0).getId();

        ReservationCustomerRequestDTO reservationCustomerRequestDTO = new ReservationCustomerRequestDTO();
        reservationCustomerRequestDTO.setPage("0");
        reservationCustomerRequestDTO.setSize("25");
        reservationCustomerRequestDTO.setSortParam("Id");
        reservationCustomerRequestDTO.setDirection("DESC");
        reservationCustomerRequestDTO.setStatuses(Arrays.stream(ReservationStatus.values())
                .map(Enum::name)
                .collect(Collectors.toList()));

        Page<ReservationResponseDTO> result = reservationFacade.getCustomerReservations(customerId, reservationCustomerRequestDTO);
        assertFalse(result.isEmpty());
        for(ReservationResponseDTO reservation: result){
            assertEquals(reservation.customerId(), customerId);
        }
    }
}
