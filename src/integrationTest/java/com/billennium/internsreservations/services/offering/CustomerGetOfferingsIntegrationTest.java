package com.billennium.internsreservations.services.offering;

import com.billennium.internsreservations.models.offering.dtos.OfferingRequestDTO;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.utils.LoadDummyData;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerGetOfferingsIntegrationTest {
    @Autowired
    OfferingService offeringService;
    @Autowired
    LoadDummyData loadDummyData;

    @BeforeAll
    public void runLoadDummyData(){
        loadDummyData.loadDummyData();
    }

    @AfterAll
    public static void cleanUpDatabase() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:myDb", "sa", "");
        Statement stmt = conn.createStatement();
        stmt.execute("DELETE FROM reservations");
        stmt.execute("DELETE FROM offerings");
        stmt.execute("DELETE FROM customers");
        stmt.execute("DELETE FROM work_hours");
        stmt.execute("DELETE FROM providers");
        stmt.execute("DELETE FROM users");
        stmt.execute("DELETE FROM addresses");
        stmt.close();
        conn.close();
    }

    @Test
    public void testPagination(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();
        offeringRequestDTO.setPage("0");
        offeringRequestDTO.setSize("10");
        offeringRequestDTO.setSortParam("Id");
        offeringRequestDTO.setDirection("DESC");
        Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        assertEquals(10, serviceOutput.getSize());
    }
    @Test
    public void testNumberOfElements(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();
        offeringRequestDTO.setPage("11");
        offeringRequestDTO.setSize("10");
        offeringRequestDTO.setSortParam("Id");
        offeringRequestDTO.setDirection("DESC");
        Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        assertEquals(2, serviceOutput.getNumberOfElements());
    }
    @Test
    public void testSorting(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();
        offeringRequestDTO.setPage("11");
        offeringRequestDTO.setSize("10");
        offeringRequestDTO.setSortParam("Id");
        offeringRequestDTO.setDirection("ASC");
        Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        List<Offering> listOutput = serviceOutput.getContent();
        for (int i = 1; i < serviceOutput.getNumberOfElements(); i++){
            assertTrue(listOutput.get(i).getId()>listOutput.get(i-1).getId());
        }
    }
    @Test
    public void setupTest(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();
        offeringRequestDTO.setPage("13");
        offeringRequestDTO.setSize("10");
        offeringRequestDTO.setSortParam("Id");
        offeringRequestDTO.setDirection("ASC");
        Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        assertEquals(112, serviceOutput.getTotalElements());
    }
    @Test
    public void outOfRangePageTest(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();
        offeringRequestDTO.setPage("14");
        offeringRequestDTO.setSize("10");
        offeringRequestDTO.setSortParam("price");
        offeringRequestDTO.setDirection("DESC");
        Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        assertEquals(0, serviceOutput.getNumberOfElements());
    }
    @Test
    public void wrongPageSizeTest(){
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            offeringRequestDTO.setPage("14");
            offeringRequestDTO.setSize("0");
            offeringRequestDTO.setSortParam("price");
            offeringRequestDTO.setDirection("DESC");
            Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        });
    }
    @Test
    public void wrongPagParamTest() {
        OfferingRequestDTO offeringRequestDTO = new OfferingRequestDTO();

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            offeringRequestDTO.setPage("14");
            offeringRequestDTO.setSize("0");
            offeringRequestDTO.setSortParam("WRONG_PARAM");
            offeringRequestDTO.setDirection("DESC");
            Page<Offering> serviceOutput = offeringService.getOfferings(offeringRequestDTO);
        });
    }
}
