package com.billennium.internsreservations.utils;

import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.utils.dbpoulation.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoadDummyData {
    private final PopulateProvider populateProvider;
    private final PopulateAddress populateAddress;
    private final PopulateOffering populateOffering;
    private final PopulateCustomer populateCustomer;
    private final PopulateWorkHour populateWorkHour;

    public void loadDummyData() {
        for (int i = 0; i < 112; i++) {
            Address address = populateAddress.insertAddress();
            Provider provider = populateProvider.insertProvider(address);
            populateWorkHour.insertWorkHours(provider);
            Offering offering = populateOffering.insertOffering(provider);
            Address address1 = populateAddress.insertAddress();
            Customer customer = populateCustomer.insertCustomer(address1);
        }
    }
}