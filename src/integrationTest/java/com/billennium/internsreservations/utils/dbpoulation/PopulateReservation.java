package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.reservation.Reservation;
import com.billennium.internsreservations.models.reservation.enums.ReservationStatus;
import com.billennium.internsreservations.repositories.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.billennium.internsreservations.utils.Helpers.getRandomNumber;
import static com.billennium.internsreservations.utils.Helpers.randomEnum;
import static java.time.LocalDateTime.now;

@RequiredArgsConstructor
@Component
public class PopulateReservation {
    private final ReservationRepository reservationRepository;
    public Reservation insertReservation(Customer customer, Offering offering){
        LocalDateTime start = now().plusHours(getRandomNumber(-1000,10000));
        Reservation reservation = new Reservation(
                offering,
                customer,
                start,
                start.plusMinutes(getRandomNumber(5,40)));
        reservation.setStatus(randomEnum(ReservationStatus.class));
        reservationRepository.save(reservation);
        return reservation;
    }
}
