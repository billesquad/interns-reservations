package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.offering.Offering;
import com.billennium.internsreservations.models.offering.enums.Currency;
import com.billennium.internsreservations.models.provider.Provider;
import com.billennium.internsreservations.repositories.OfferingRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@RequiredArgsConstructor
@Component

public class PopulateOffering {
    Faker faker = new Faker();
    private final OfferingRepository offeringRepository;

    public Offering insertOffering(Provider provider){
        Offering offering = new Offering(
                provider,
                faker.company().buzzword(),
                faker.random().nextInt(5, 60),
                ThreadLocalRandom.current().nextDouble(1, 3000),
                Currency.PLN);
        offeringRepository.save(offering);
        return offering;
    }

}
