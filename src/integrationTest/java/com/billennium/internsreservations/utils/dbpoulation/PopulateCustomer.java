package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.models.customer.Customer;
import com.billennium.internsreservations.repositories.CustomerRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Random;

import static com.billennium.internsreservations.utils.Helpers.NO_TOKEN;

@RequiredArgsConstructor
@Component
public class PopulateCustomer {

    Faker faker = new Faker();
    public final CustomerRepository customerRepository;

    public Customer insertCustomer(Address address){
        Customer customer = new Customer(
                address,
                faker.name().firstName(),
                faker.name().lastName(),
                faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.internet().emailAddress(),
                new Random().ints(9, 0, 10)
                        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString(),
                NO_TOKEN,
                NO_TOKEN,
                NO_TOKEN);
        customerRepository.save(customer);
        return customer;
    }
}
