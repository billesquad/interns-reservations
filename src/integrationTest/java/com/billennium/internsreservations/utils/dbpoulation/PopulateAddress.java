package com.billennium.internsreservations.utils.dbpoulation;

import com.billennium.internsreservations.models.address.Address;
import com.billennium.internsreservations.repositories.AddressRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@RequiredArgsConstructor
@Component
public class PopulateAddress {
    Faker faker = new Faker();
    private final AddressRepository addressRepository;

    public Address insertAddress(){
        Address address = new Address();
        address.setCountry(faker.address().country());
        address.setCity(faker.address().city());
        address.setStreet(faker.address().streetName());
        address.setZipCode(faker.address().zipCode());
        address.setApartmentNumber(String.valueOf(ThreadLocalRandom.current().nextInt(1, 100)));
        address.setApartmentNumber(String.valueOf(ThreadLocalRandom.current().nextInt(1, 100)));
        addressRepository.save(address);
        return address;

    }

}
