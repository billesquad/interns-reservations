plugins {
	java
	id("org.springframework.boot") version "3.1.2"
	id("io.spring.dependency-management") version "1.1.2"
	id("org.flywaydb.flyway") version "9.20.1"
	id("io.freefair.lombok") version "8.1.0"
}

group = "com.billennium"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_20
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {

	mavenCentral()

	maven {
		url = uri("https://repo.spring.io/release")
	}
	maven {
		url = uri("https://repository.jboss.org/maven2")
	}

}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	compileOnly("org.projectlombok:lombok:1.18.28")
	annotationProcessor ("org.projectlombok:lombok:1.18.28")
	implementation("javax.persistence:javax.persistence-api:2.2")
	runtimeOnly("org.postgresql:postgresql")
	implementation("org.flywaydb:flyway-core:9.20.1")
	implementation ("org.hibernate.validator:hibernate-validator:7.0.1.Final")
	implementation ("jakarta.validation:jakarta.validation-api:3.0.0")
	implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.1.0")
	implementation("com.github.javafaker:javafaker:1.0.2")
	implementation("io.jsonwebtoken:jjwt-api:0.11.5")
	implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
	implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
	implementation("com.google.api-client:google-api-client:2.2.0")
	implementation("com.google.oauth-client:google-oauth-client:1.34.1")
	implementation("com.google.apis:google-api-services-oauth2:v2-rev157-1.25.0")
	implementation("com.google.http-client:google-http-client-jackson2:1.43.3")
	implementation("com.auth0:java-jwt:4.4.0")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

flyway {
	url = "jdbc:postgresql://localhost:5432/interns-reservation-db"
	user = "postgres"
	password = "postgres"
}

testing {
	suites {
		register<JvmTestSuite>("integrationTest") {
			dependencies {
				implementation(project())
				implementation("com.h2database:h2:2.2.220")
				implementation("org.springframework.boot:spring-boot-starter")
				implementation("org.springframework.boot:spring-boot-starter-data-jpa")
				implementation("org.springframework.boot:spring-boot-starter-web")
				implementation("org.springframework.boot:spring-boot-starter-test")
				compileOnly("org.projectlombok:lombok:1.18.28")
				annotationProcessor ("org.projectlombok:lombok:1.18.28")
				implementation("javax.persistence:javax.persistence-api:2.2")
				implementation ("org.hibernate.validator:hibernate-validator:7.0.1.Final")
				implementation ("jakarta.validation:jakarta.validation-api:3.0.0")
				implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.1.0")
				implementation("com.github.javafaker:javafaker:1.0.2")
				implementation("io.jsonwebtoken:jjwt-api:0.11.5")
				implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
				implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")
				implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
				implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
				implementation("com.google.api-client:google-api-client:2.2.0")
				implementation("com.google.oauth-client:google-oauth-client:1.34.1")
				implementation("com.google.apis:google-api-services-oauth2:v2-rev157-1.25.0")
				implementation("com.google.http-client:google-http-client-jackson2:1.43.3")
				implementation("com.auth0:java-jwt:4.4.0")
				implementation("org.springframework.boot:spring-boot-starter-test")
				implementation("org.springframework.security:spring-security-test")
			}
		}
	}
}

tasks.named("check") {
	dependsOn(testing.suites.named("integrationTest"))
}

tasks.register<JavaExec>("loadData") {
	classpath = sourceSets["main"].runtimeClasspath
	mainClass = "com.billennium.internsreservations.InternsReservationsApplication"
	args("--spring.profiles.active=loadData")
}